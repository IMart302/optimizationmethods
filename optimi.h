#ifndef OPTIM_H
#define OPTIM_H

#include "function.h"
#include "tools.h"
#include <cmath>
#include <cfloat>
#include <iostream>
#include <ctime>
#include <fstream>


//clase Line Search Implemeta varios tipos de busqueda de linea
class LineSearch
{
protected: 
    // 0 == backtracking
    // 1 == interpolacion
    // 2 == soft line
    // other == exact line
    int type;
public:

    double alfa_default; //Alfa por defecto si no hay busqueda de linea

    LineSearch(int t){
        this->type = t;
        alfa_default = 0.001;
    }

    void find(double* xold, double fold, double* p, double* g, double* x, double* f, FunctionND* func, bool numerical = false){
        if(this->type == -1){
            //NO HAY BUSQUEDA DE LINEA SOLO DA EL PASO
            dcopy_(func->dimIn(), xold, 1, x, 1);
            daxpy_(func->dimIn(), alfa_default, p, 1, x, 1);      // x = x + alfa*p
            func->eval(x, f);
            func->jaco(x, g, numerical);
        }
        else if(this->type == 0){
            backtracking(xold, fold, p, g, x, f, func, numerical);
        }
        else if(this->type == 1){
            interpol(xold, fold, p, g, x, f, func, numerical);
        }
        else if(this->type == 2){
            softline(xold, fold, p, g, x, f, func, numerical);
        }
        else{
            exactline(xold, fold, p, g, x, f, func, numerical);
        }
    }

    void linesearch(bool flagW, double* xold, double fold, double* p, double* g, double* x, double* f, FunctionND* func, bool numerical = false){
        int N = func->dimIn();
        // variables de control
        double epsilon = 1.0e-6, tau = 1.0e-6, rho = 1.0e-2, beta = 1.0e-1, alfaMax = 10.0;
        int kmax = 10;

        // Checa condiciones de descenso
        dcopy_( N, xold, 1, x, 1 );
        //(*func)( param, x, f, g );
        func->eval(x, f);
        func->jaco(x, g, numerical);
        double CondD = -10.0*DBL_EPSILON*dnrm2_(N, g, 1)*dnrm2_(N, p, 1);
        double dphi = ddot_( N, g, 1, p, 1 );
        if(dphi >= CondD)
            return;

        // constantes de la busqueda
        int k = 0;
        double slope0, slopethr, lambda;
        if(flagW)                       // condiciones fuertes de Wolfe
        {
            slope0 = 0.0;                      
            slopethr = tau*fabs(dphi);
        }
        else                                   // condiciones suaves de Wolfe
        {
            slope0 = rho*dphi;               // Armijo
            slopethr = beta*dphi;          // curvatura
        }

        // valores iniciales
        double a = 0.0, Fa = f[0], dphia = dphi;
        double b = (1.0 > alfaMax) ? alfaMax : 1.0;    // b
        double Fb, dphib;
    

        // localiza regiones de valores optimos de alpha
        bool stop = false;
        while ( !stop )
        {
            // incrementa contador de busqueda
            k++;

            // calcula nuevo rango de alpha
            dcopy_( N, xold, 1, x, 1 );
            daxpy_( N, b, p, 1, x, 1 );
            //(*func)( param, x, &Fb, g );                  // F(b)
            func->eval(x, &Fb);
            func->jaco(x, g, numerical);            
            dphib = ddot_( N, g, 1, p, 1 );      // deriv phi(b)
            lambda = fold + slope0*b;

            // prueba condicion de Armijo: descenso
            if(Fb < lambda)
            {
                // guarda valores de potencial region
                if ( !flagW )
                    {    a = b;    Fa = Fb;    dphia = dphib;    }
                dcopy_( N, xold, 1, x, 1 );
                daxpy_( N, b, p, 1, x, 1 );
                f[0] = Fb;

                // prueba condicion de curvatura
                if ( (dphib < ((slopethr>0.0) ? 0.0 : slopethr )) && (k < kmax) && (b < alfaMax) )
                {
                    // busca un valor optimo
                    if ( flagW )
                        {    a = b;    Fa = Fb;    dphia = dphib;    }
                    if ( 2.5*b >= alfaMax )
                        b = alfaMax;
                    else
                        b *= 2.0;
                }
                else
                    stop = true;
            }
            else
                stop = true;
        }

        // refina valores de alpha, prueba criterios de Wolfe
        stop = ( k > kmax) || ( (b >= alfaMax) && (dphib < slopethr) ) || ( !flagW && ( (a > 0) && (dphib >= slopethr) ) );
        while ( !stop )
        {
            // interpola valor de alpha
            double alfa, dphi, D = b - a;
            double c = Fb - Fa - D*dphia;
            if ( c > 0.0f )
            {
                alfa = a - 0.5*dphia*D*D/c;
                double va = a + 0.1*D, vb = b - 0.1*D;
                double val = ( alfa > va ) ? alfa : va;
                alfa = ( val < vb ) ? val : vb;
            }
            else
                alfa = 0.5*(a + b);       

            // incrementa contador de busqueda
            k++;

            // calcula valor optimo en rango de busqueda
            dcopy_( N, xold, 1, x, 1 );
            daxpy_( N, alfa, p, 1, x, 1 );      // xn = xo + c*p
            //(*func)( param, x, f, g );    // F(c)
            func->eval(x, f);
            func->jaco(x, g, numerical);
            dphi = ddot_( N, g, 1, p, 1 );      // deriv phi(c)
            if ( !flagW )
            {
                // prueba curvatura, respalda valores
                if ( *f < (fold + slope0*alfa) )
                {
                    dcopy_( N, xold, 1, x, 1 );
                    daxpy_( N, alfa, p, 1, x, 1 );
                    a = alfa;    Fa = *f;    dphia = dphi;
                    stop = (dphi > slopethr); 
                }
                else
                {    b = alfa;    Fb = *f;    dphib = dphi;    }
            }
            else
            {
                // prueba descenso, respalda valores si 
                if ( *f < f[0] )
                {
                    dcopy_( N, xold, 1, x, 1 );
                    daxpy_( N, alfa, p, 1, x, 1 );
                }
                if ( dphi < 0.0 )
                {    a = alfa;    Fa = *f;    dphia = dphi;    }
                else
                {    b = alfa;    Fb = *f;    dphib = dphi;    }

                // prueba condiciones de paro
                stop = (fabs(dphi) <= slopethr ) || ((b-a) <= epsilon*b); 
            }
            stop = stop || (k >= kmax);
        }
    }

    void softline(double* xold, double fold, double* p, double* g, double* x, double* f, FunctionND* func, bool numerical = false){
        this->linesearch(false, xold, fold, p, g, x, f, func, numerical);
    }

    void exactline(double* xold, double fold, double* p, double* g, double* x, double* f, FunctionND* func, bool numerical = false){
        this->linesearch(true, xold, fold, p, g, x, f, func, numerical);
    }

    //algoritmo de interpolacion
    void interpol(double* xold, double fold, double* p, double* g, double* x, double* f, FunctionND* func, bool numerical = false){
        // Algorithm interpolacion, libro Nocedal 2a edicion
        int N = func->dimIn();
        double c = 0.45;
        double dphio = ddot_( N, g, 1, p, 1 ); 
        double F0 = fold, dphi0 = dphio, dphi = dphio;
        dphio = c*dphio;
        double alfa0 = 2.0;
        double alfa = alfa0;
        bool flag = true;
        double* F = f;
        double Fo = fold;
        //double up;
        //double down;

        dcopy_( N, xold, 1, x, 1 );            // actualiza valor
        daxpy_( N, alfa, p, 1, x, 1 );      // nueva posicion
        
        //std::cout << "interpol" << std::endl;
        //Func(funparams, x, &F, g, funevals);          // estima nuevo gradiente

        func->eval(x, F);
        func->jaco(x, g, numerical);
        
        dcopy_(N, g, 1, p, 1);
        dscal_(N, -1.0, p, 1);
        dphi = ddot_( N, g, 1, p, 1 );
        while ( *F > (Fo + alfa*dphio) )   // prueba condicion Armijo
        {
            if (flag == true)
            {
                // interpolacion cuadratica
                //up = (-alfa)*dphi;
                //down = 2.0*(dphi - (*F - F0 - 1.0)/(-alfa - 1.0));
                alfa = ((-dphi0*alfa*alfa) / (2.0*(*F-F0-dphi0*alfa)));
                //alfa = alfa - up/down;
                flag = false;
                //std::cout << alfa << " ";
            }
            else
            {
                // interpolacion cubica
                double v1 = alfa - alfa0;
                double d1 = dphi0 + dphi - 3.0*(F0- *F) / (-v1);
                double d2 = float((v1 > 0) - (v1 < 0))*sqrt(d1*d1 - dphi0*dphi);
                double temp = alfa - v1*(dphi + d2 - d1) / (dphi - dphi0 + 2.0*d2);
                alfa0 = alfa;    alfa = temp;    F0 = *F;    dphi0 = dphi;            // respalda valores
            }
            //F0 = *F;
            dcopy_( N, xold, 1, x, 1 );          // actualiza valor
            daxpy_( N, alfa, p, 1, x, 1 );    // nueva posicion
            //Func(funparams, x, &F, g, funevals);         // estima nuevo gradiente
            func->eval(x, F);
            func->jaco(x, g, numerical);
            //dcopy_(N, g, 1, p, 1);
            //dscal_(N, -1.0, p, 1);
            //dphi = ddot_( N, g, 1, p, 1 );    // nva derivada direccional
        }
    }

    void backtracking(double* xold, double fold, double* p, double* g, double* x, double* f, FunctionND* func, bool numerical = false){
        int N = func->dimIn();
        // variables 
        double Fo = fold;
        double alfa0 = 2.8;
        double alfa = alfa0;
        double ro = 0.8;
        double c = 0.45;
        double dphio = c*ddot_(N, g, 1, p, 1);
        dcopy_(N, xold, 1, x, 1 );            // actualiza valor
        daxpy_(N, alfa, p, 1, x, 1 );         // nueva posicion
        double* F = f;
        //Func(funparams, x, &F, g , funevals);          // estima nuevo gradiente
        func->eval(x, F);
        func->jaco(x, g, numerical);

        while (*F > (Fo + alfa*dphio) )   // prueba condicion Armijo
        {
            alfa = ro*alfa; 
            dcopy_(N, xold, 1, x, 1 );          // actualiza valor
            daxpy_(N, alfa, p, 1, x, 1 );       // nueva posicion
            //Func(funparams, x, &F, g, funevals);         // estima nuevo gradiente
            func->eval(x, F);
            func->jaco(x, g, numerical);
        }
    }
};

//Clases auxiliares para facilitar las busqueda de linea
//y no recordar el type en la clase base
class ExactLineSearch : public LineSearch{
public: 
    ExactLineSearch(): 
        LineSearch(3)
    {

    }
};

class SoftLineSearch : public LineSearch{
public: 
    SoftLineSearch(): 
        LineSearch(2)
    {

    }
};

class InterLineSearch : public LineSearch{
public: 
    InterLineSearch(): 
        LineSearch(1)
    {

    }
};

class BacktrackLineSearch : public LineSearch{
public: 
    BacktrackLineSearch(): 
        LineSearch(0)
    {

    }
};

class NoneLineSearch : public LineSearch{
public: 
    NoneLineSearch(): 
        LineSearch(-1)
    {

    }
};


//Clase para definir criterios de paro
class StopCriteria{
protected: 
    double normdX;
    double normG;
    double normX;
    double dF;

public:
    double epsilon1;
    double epsilon2;
    double epsilon3;
    int maxiter;
    int crit;
    double* temp;

    ~StopCriteria(){
        delete temp;
    }

    void reserveMem(int N){
        temp = new double[N];
    }

    StopCriteria(){
        epsilon1 = 1.0e-10;
        epsilon2 = 1.0e-12;
        epsilon3 = 1.0e-12;
        maxiter = 10000;
        crit = 0;
        temp = NULL;
    }

    StopCriteria(double eps1, double eps2, double eps3, int maxit, int criteria){
        epsilon1 = eps1;
        epsilon2 = eps2;
        epsilon3 = eps3;

        maxiter = maxit;
        crit = criteria;
        temp = NULL;
    }

    bool check(int N, int it, double* xo, double* fold, double* x, double* f, double* g){
        if(crit == 0){
            //obtiene variables de criterios de paro
            dcopy_( N, xo, 1, temp, 1 );
            daxpy_( N, -1.0, x, 1, temp, 1 );
            double normdX = dnrm2_( N, temp, 1 );        // diferencias entre puntos
            dF = fabs(*fold - *f);
            normG = dnrm2_(N, g, 1);            // norma euclidiana gradiente
            return crit1(it, dF, normdX, normG, 0.0);
        }
        else{
            dcopy_( N, xo, 1, temp, 1 );
            daxpy_( N, -1.0, x, 1, temp, 1 );
            normdX = dnrm2_(N,temp,1);    // diferencias entre puntos
            int pos = idamax_(N,g,1) - 1;
            normG = fabs( g[pos] );            // norma inf gradiente
            normX = dnrm2_(N,x,1);
            //dF = fabs(*f - *fold);
            return crit2(it, dF, normdX, normG, normX);
        }
    }

    //bool check(int it, double dF, double normdX, double normG, double normX){
    //    if(crit == 0){
    //        return crit1(it, dF, normdX, normG, normX);
    //    }
    //    else{
    //        return crit2(it, dF, normdX, normG, normX);
    //    }
    //}

    bool crit1(int it, double dF, double normdX, double normG, double normX){
        return ((it < maxiter) && (normdX > epsilon1) && (dF > epsilon1*(normdX)) && (normG > epsilon2));
    }

    bool crit2(int it, double dF, double normdX, double normG, double normX){
        return ((it < maxiter) && (normG > epsilon1) && (normdX > epsilon2*(epsilon2+normX)));
    }
};


//Clase optimizador, implementa varios metodos de optimizacion sin restricciones para funciones R^n -> R
class Optim{
public:

    //Hace que los metodos no despliegen mensajes en la consola
    //Por defecto es false
    static bool quiet;

    //Guarda el ultimo valor de iteraciones del algoritmo ejecutado
    static int iter_mem;

    //Descenso del gradiente estandar
    static void GradientDescend(FunctionND* funct, 
                                double* xinit, 
                                LineSearch* lsearch, 
                                StopCriteria* crite, 
                                std::ofstream &file, 
                                bool numerical = false)
    {
        // declara variables de computo
        int N = funct->dimIn();
        double* xo = (double*) vectorr(N,sizeof(double));
        double* x = (double*) vectorr(N,sizeof(double));
        double* g = (double*) vectorr(N,sizeof(double));
        double* p = (double*) vectorr(N,sizeof(double));
        double* temp = (double*) vectorr(N,sizeof(double));
        double Fo, F;
  
        // valores iniciales  
        dcopy_(N, xinit, 1, xo, 1);
        //xo[0] = -1.2;    xo[1] = 1.0;          // posicion inicial
        //Func( dummy, xo, &Fo, g, numFunEval);  // gradiente incial
        funct->eval(xo, &Fo);
        funct->jaco(xo, g, numerical);
  
        // guarda secuencia
        
        file << "#x0 to xn | n=" << N <<", F , ||dF||" << std::endl;
        //file << xo[0] << "  " << xo[1] << "  " << dnrm2_(N,g,1) << endl;
        file << 0 << " ";
        for(int n = 0; n < N; n++)
            file << xo[n] << " ";
        file << Fo << " " << dnrm2_(N, g, 1) << std::endl;

        // parametros para criterios de paro
        //double epsilon1 = 1.0e-6, epsilon2 = 1.0e-12, normG, dF, normdX;
        //double normG, dF, normdX, normX;
        double normG;
        bool crit = true;
        //unsigned kmax = 50000;  
  
        // inicia iteraciones 
        unsigned k = 0;
        do 
        {
            // obtiene direccion de descenso
            dcopy_( N, g, 1, p, 1 );
            dscal_( N, -1.0, p, 1);


            // busca el valor optimo en la direccion de descenso
            // Algorithm interpolacion, libro Nocedal 2a edicion
            //DG30(alfa, N, x, xo, p, g, F, Fo, dummy, numFunEval);
            lsearch->find(xo, Fo, p, g, x, &F, funct, numerical);

            // obtiene variables de criterios de paro
            //dcopy_( N, xo, 1, temp, 1 );
            //daxpy_( N, -1.0, x, 1, temp, 1 );
            //normdX = dnrm2_( N, temp, 1 );        // diferencias entre puntos
            //dF = Fo - F;
            normG = dnrm2_( N, g, 1 );            // norma euclidiana gradiente
            
            crit = crite->check(N, k, xo, &Fo, x, &F, g);
      
            // guarda secuencia
            //file << x[0] << "  " << x[1] << "  " << normG << endl;     // guarda datos
            file << k+1 << " ";
            for(int n = 0; n < N; n++)
                file << x[n] << " ";
            file << F << " " << normG << std::endl;

            // respalda valores para prox. iteraccion, incrementa contador 
            Fo = F;
            dcopy_( N, x, 1, xo, 1 );    // posicion
            k++;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if (k%100 == 0)
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        } while (crit);
        
        Optim::iter_mem = k;
        //std::cout << "iteraciones: "<< k << std::endl;

        free_vectorr( (void*) x );
        free_vectorr( (void*) xo );
        free_vectorr( (void*) g );
        free_vectorr( (void*) p );
        free_vectorr( (void*) temp );
    }

    //Gradiente Conjugado, por defecto usa Polak-Ribiere (pr_fr = false), sino usa Fletcher-Rieves (pr_fr = true)
    static void ConjugateGradient(FunctionND* funct, 
                                  double* xinit, 
                                  LineSearch* lsearch, 
                                  StopCriteria* crite, 
                                  std::ofstream &file, 
                                  bool pr_fr = false, 
                                  bool numerical = false)
    {
        // declara variables de computo
        int N = funct->dimIn();
        double* xo = (double*) vectorr(N,sizeof(double));
        double* x = (double*) vectorr(N,sizeof(double));
        double* g = (double*) vectorr(N,sizeof(double));
        double* h = (double*) vectorr(N,sizeof(double));
        double* temp = (double*) vectorr(N,sizeof(double));  
        double Fo, F;
  
        // valores iniciales  
        //xo[0] = -1.2;    xo[1] = 1.0;      // posicion inicial
        dcopy_(N, xinit, 1, xo, 1);
        //Func( dummy, xo, &Fo, g );        // gradiente incial
        funct->eval(xo, &Fo);
        funct->jaco(xo, g, numerical);
  
        // guarda secuencia
        
        file << "#k, x1 to xn | n=" << N <<", F , ||dF||" << std::endl;
        //file << xo[0] << "  " << xo[1] << "  " << dnrm2_(N,g,1) << endl;
        file << 0 <<" ";
        for(int n = 0; n < N; n++)
            file << xo[n] << " ";
        file << Fo << " " << dnrm2_(N, g, 1) << std::endl;
  
        // parametros para criterios de paro
        //double epsilon1 = 1.0e-6, epsilon2 = 1.0e-12, normG, normdX, normX;
        //double normG, normdX, normX, dF;
        double normG;
        bool crit = true;
        //unsigned kmax = 500000;  
  
  
        // inicia iteraciones 
        unsigned k = 0;
        double gamma = 0.0;
        do 
        {
             // obtiene direccion de descenso
            dscal_( N, gamma, h, 1);
            daxpy_( N, -1.0, g, 1, h, 1 );
            if ( ddot_(N,g,1,h,1) >= 0.0 )
            {
                dcopy_( N, g, 1, h, 1 );
                dscal_( N, -1.0, h, 1);
            }

            // busca en la direccion de descenso el valor optimo
            double old = ddot_(N,g,1,g,1);    // respalda valor 
            dcopy_( N, g, 1, temp, 1 );          // copia vector 
            //lnsrch( false, N, xo, Fo, h, g, x, &F, Func, dummy );
            lsearch->find(xo, Fo, h, g, x, &F, funct, numerical);
            double cte = -1.0;          // cte = 0, Fletcher-Reeves formula; cte = -1, Polak-Ribiere formula 
            if(pr_fr)
                cte = 0.0;       
            dscal_( N, cte, temp, 1);             // PR formula o FR formula
            daxpy_( N, 1.0, g, 1, temp, 1 );
            gamma = ddot_(N,temp,1,g,1) / old;    // parametros del metodo

            // obtiene variables de criterios de paro
            //dcopy_( N, xo, 1, temp, 1 );
            //daxpy_( N, -1.0, x, 1, temp, 1 );
            //normdX = dnrm2_(N,temp,1);    // diferencias entre puntos
            //int pos = idamax_(N,g,1) - 1;
            //normG = fabs( g[pos] );            // norma inf gradiente
            //normX = dnrm2_(N,x,1);
            //dF = fabs(F - Fo);
            normG = dnrm2_(N, g, 1);
            crit = crite->check(N, k, xo, &Fo, x, &F, g);

            // guarda secuencia
            //file << x[0] << "  " << x[1] << "  " << normG << std::endl;     // guarda datos
            file << k+1 <<" ";
            for(int n = 0; n < N; n++)
                file << xo[n] << " ";
            file << Fo << " " << normG << std::endl;

            // respalda valores para prox. iteraccion, incrementa contador 
            Fo = F;
            dcopy_( N, x, 1, xo, 1 );    // posicion
            k++;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if (k%100 == 0)
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        } while (crit);
        
        Optim::iter_mem = k;

        free_vectorr( (void*) x );
        free_vectorr( (void*) xo );
        free_vectorr( (void*) g );
        free_vectorr( (void*) h );
        free_vectorr( (void*) temp );
    }

    //Metodo de Newton
    static void NewtonMethod(FunctionND* funct, 
                             double* xinit, 
                             StopCriteria* crite, 
                             std::ofstream& file, 
                             bool numerical = false)
    {
        // declara variables de computo
        int N = funct->dimIn();
        double* xo = (double*) vectorr(N,sizeof(double));
        double* x = (double*) vectorr(N,sizeof(double));
        double* g = (double*) vectorr(N,sizeof(double));
        double* H = (double*) vectorr(N*N, sizeof(double));
        double* temp = (double*) vectorr(N,sizeof(double));
        int* ipiv = (int*) vectorr(N, sizeof(int));
        double Fo, F;
        int info;
  
        // valores iniciales  
        //xo[0] = 1.0;    xo[1] = 0.7;      // posicion inicial
        //Func(NULL, xo, &Fo, g, H);        // gradiente incial
        dcopy_(N, xinit, 1, xo, 1);
        dcopy_(N, xo, 1, x, 1);
        funct->eval(x, &F);
        funct->jaco(x, g, numerical);
        funct->hess(x, H, numerical);
        Fo = F;

        // guarda secuencia
        file << "#x1 to xn | n=" << N <<", F , ||dF||" << std::endl;
        //file << xo[0] << "  " << xo[1] << "  " << dnrm2_(N,g,1) << endl;
        file << 0 <<" ";
        for(int n = 0; n < N; n++)
            file << xo[n] << " ";
        file << Fo << " " << dnrm2_(N, g, 1) << std::endl;
  
        // parametros para criterios de paro
        //double normG, dF, normdX, normX;
        double normG;
        bool crit = true;
        
  
        // inicia iteraciones 
        unsigned k = 0;
        do 
        {

            dscal_( N, -1.0, g, 1);
            dgesv_(N, 1, H, N, ipiv, g, N, info);
            if(info != 0){
                std::cout << "Error en la solucion del sistema lineal\n" << std::endl;
                return;
            }
            daxpy_(N, 1.0, g, 1, x, 1);
            //Func(NULL, x, &F, g, H); 
            funct->eval(x, &F);
            funct->jaco(x, g, numerical);
            funct->hess(x, H, numerical);


            // obtiene variables de criterios de paro
            //dcopy_( N, xo, 1, temp, 1 );
            //daxpy_( N, -1.0, x, 1, temp, 1 );
            //normdX = dnrm2_( N, temp, 1 );    // diferencias entre puntos
            //int pos = idamax_(N, g, 1);
            //normG = fabs(g[pos]);           // norma euclidiana gradiente
            //normX = dnrm2_(N,x,1);
            //dF = fabs(F - Fo);
            normG = dnrm2_(N, g, 1);
            crit = crite->check(N, k, xo, &Fo, x, &F, g);

            Fo = F;
            // guarda secuencia
            file << k+1 <<" ";
            for(int n = 0; n < N; n++)
                file << x[n] << " ";
            file << F << " " << dnrm2_(N, g, 1) << std::endl;

            // respalda valores para prox. iteraccion, incrementa contador 
            //Fo = F;
            dcopy_( N, x, 1, xo, 1 );    // posicion
            k++;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if ( k%100 == 0 )
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        }while(crit);

        Optim::iter_mem = k;

        free_vectorr((void*) xo);
        free_vectorr((void*) x);
        free_vectorr((void*) g);
        free_vectorr((void*) H);
        free_vectorr((void*) temp);
        free_vectorr((void*) ipiv);
    }
    

    //Metodo de newton amortiguado (Levenberg-Marquad)
    static void DampedNewton(FunctionND* funct, 
                             double* xinit, 
                             StopCriteria* crite, 
                             std::ofstream& file, 
                             bool numerical = false)
    {
        // declara variables de computo
        int N = funct->dimIn();
	    double* xo = (double*) vectorr(N,sizeof(double));
	    double* x = (double*) vectorr(N,sizeof(double));
	    double* g = (double*) vectorr(N,sizeof(double));
	    double* go = (double*) vectorr(N,sizeof(double));
	    double* h = (double*) vectorr(N,sizeof(double));
	    double* H = (double*) vectorr(N*N,sizeof(double));
	    double* Hf = (double*) vectorr(N*N,sizeof(double));
	    double* I = (double*) vectorr(N*N,sizeof(double));
	    double* temp = (double*) vectorr(N,sizeof(double));	
	    double Fo, F;
	
	    // valores iniciales	
	    //xo[0] = 1.0;	xo[1] = .7;		// posicion inicial
	    //xo[0] = 1.0;	xo[1] = 2.0;		// posicion inicial CRASH
        dcopy_(N, xinit, 1, xo, 1);
	    //Func( dummy, xo, &F, g, H );		// evaluacion inicial
        funct->eval(xo, &F);
        funct->jaco(xo, g, numerical);
        funct->hess(xo, H, numerical);
	    dscal_(N*N, 0.0,I,1);
	    for(int r = 0; r < N; r++)
	    {
		    I[r*N +r] = 1.0;
	    }
	
	    // guarda secuencia
        file << "#x1 to xn | (n=" << N <<"), F , ||dF||" << std::endl;
        file << 0 <<" ";
	    for(int n = 0; n < N; n++)
                file << xo[n] << " ";
        file << F << " " << dnrm2_(N, g, 1) << std::endl;
	
	    // parametros para criterios de paro
	    //double epsilon1 = 1.0e-8, epsilon2 = 1.0e-12, normG, normdX, normX;
        //double normG, normdX, normX, dF;
        double normG;
        bool crit = true;
	    //int kmax = 500000;	
	
	    // inicia iteraciones 
	    dcopy_(N,xo,1,x,1); //Posicion inicial
	    double mu = 1.0;
	    int info, k = 0;
	    do
	    {
		    bool flag = false;
		    do
		    {
			    dcopy_(N*N, H, 1, Hf, 1);
			    daxpy_(N*N, mu, I, 1, Hf, 1);
			    dpotrf_('L', N, Hf, N, info);
			    if(info == 0)
			    {
			    	flag = true;
			    }else
			    {
			    	mu *= 2.0;
			    }
		    }while(!flag);
		    dcopy_(N, g, 1, h, 1);
		    dscal_(N, -1.0, h, 1);
		    dpotrs_('L', N, 1, Hf, N, h, N, info);
		    if(info != 0)
		    {
		    	std::cout<<"error en la solucion del sistema lineal"<<std::endl;
                return;
		    }

		    Fo = F;
		    dcopy_(N, g, 1, go, 1);
		    daxpy_(N, 1.0, h, 1, x, 1);
		    //Func(dummy, x, &F, g, H);
            funct->eval(x, &F);
            funct->jaco(x, g, numerical);
            funct->hess(x, H, numerical);
		    double r = (Fo- F)/(0.5*mu*ddot_(N, h, 1, h, 1)- 0.5*ddot_(N, g, 1, h, 1));
		    if( r > 1.0e-3)
		    {
			    double valor = 1.0 - pow((2.0*r - 1.0), 3.0);
			    mu *= (valor > 1.0/3.0) ? valor : 1.0/3.0;
		    }
		    else
		    {
			    dcopy_(N, xo, 1, x, 1);
			    dcopy_(N, go, 1, g, 1);
			    mu *= 2.0;
		    }


		    // obtiene variables de criterios de paro
		    //dcopy_( N, xo, 1, temp, 1 );
		    //daxpy_( N, -1.0, x, 1, temp, 1 );
		    //normdX = dnrm2_(N,temp,1);	// diferencias entre puntos
		    //normX = dnrm2_(N,x,1);
		    //int pos = idamax_(N,g,1) - 1;
		    //normG = fabs( g[pos] ); // norma inf gradiente
            //dF = fabs(F - Fo);

            normG = dnrm2_(N, g, 1);
            crit = crite->check(N, k, xo, &Fo, x, &F, g);
		
            //Guarda secuencia
            file << k+1 <<" ";
	        for(int n = 0; n < N; n++)
                file << x[n] << " ";
            file << F << " " << normG << std::endl;

		    // respalda valores para prox. iteraccion, incrementa contador 
		    Fo = F;
		    dcopy_( N, x, 1, xo, 1 );	// posicion
		    k++;

		    // despliega resultados cada n-iteraciones
		    if(!Optim::quiet){
                if (k%100 == 0)
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }

	    }while(crit);

        Optim::iter_mem = k;

        //std::cout << "iteraciones: "<< k << std::endl;
        free_vectorr( (void*) xo);
        free_vectorr( (void*) x);
        free_vectorr( (void*) g);
        free_vectorr( (void*) go);
        free_vectorr( (void*) h);
        free_vectorr( (void*) H);
        free_vectorr( (void*) Hf);
        free_vectorr( (void*) I);
        free_vectorr( (void*) temp);
    }

    //Metodo quasi Newton DFP version by default (dfp = false), otherwise use bfgs (dfp_bfgs = true)
    static void QuasiNewton(FunctionND* funct, 
                            double* xinit, 
                            LineSearch* lsearch, 
                            StopCriteria* crite, 
                            std::ofstream& file, 
                            bool dfp_bfgs = false, 
                            bool numerical = false)
    {
        // declara variables de computo
        int N = funct->dimIn();
	    double* xo = (double*) vectorr(N,sizeof(double));
	    double* x = (double*) vectorr(N,sizeof(double));
        double* y = (double*) vectorr(N, sizeof(double));
	    double* g = (double*) vectorr(N,sizeof(double));
	    double* go = (double*) vectorr(N,sizeof(double));
	    double* h = (double*) vectorr(N,sizeof(double));
        double* v = (double*) vectorr(N, sizeof(double));
	    //double* H = (double*) vectorr(N*N,sizeof(double));
	    //double* Hf = (double*) vectorr(N*N,sizeof(double));
	    double* D = (double*) vectorr(N*N, sizeof(double));
	    double* temp = (double*) vectorr(N,sizeof(double));	
	    double Fo, F;
	
	    // valores iniciales
        dcopy_(N, xinit, 1, xo, 1);
	    //Func( dummy, xo, &F, g, H );		// evaluacion inicial
        funct->eval(xo, &F);
        funct->jaco(xo, g, numerical);
	
        dscal_(N*N, 0.0, D, 1);
        for(int r = 0; r<N; r++){
            D[r*N+r] = 1.0;
        }
	    // guarda secuencia
	    // guarda secuencia
        file << "#x1 to xn | n=" << N <<", F , ||dF||" << std::endl;
        //file << xo[0] << "  " << xo[1] << "  " << dnrm2_(N,g,1) << endl;
        file << 0 <<" ";
        for(int n = 0; n < N; n++)
            file << xo[n] << " ";
        file << F << " " << dnrm2_(N, g, 1) << std::endl;
	
	    // parametros para criterios de paro
	    //double epsilon1 = 1.0e-8, epsilon2 = 1.0e-12, normG, normdX, normX;
        //double normG, normdX, normX;
        double normG;
        bool crit = true;
	    //int kmax = 500000;	
	
	    // inicia iteraciones 
	    dcopy_(N,xo,1,x,1); //Posicion inicial
	    int k = 0;
	    do
	    {
		    dsymv_("U", N, -1.0, D, N, g, 1, 0.0, h, 1);

            dcopy_(N, x, 1, xo, 1);
            dcopy_(N, g, 1, go, 1);

            Fo = F;
            //lnsrch(false, N, xo, Fo, h, g, x, &F, Func, dummy);
            lsearch->find(xo, Fo, h, g, x, &F, funct, numerical);

            dcopy_(N, x, 1, h, 1);
            daxpy_(N, -1.0, xo, 1, h, 1);
            dcopy_(N, g, 1, y, 1);
            daxpy_(N, -1.0, go, 1, y, 1);

            if(ddot_(N, h, 1, y, 1) > (sqrt(DBL_EPSILON)*dnrm2_(N, h, 1)*dnrm2_(N, y, 1))){
            
                dsymv_("U", N, 1.0, D, N, y, 1, 0.0, v, 1);
                double k2 = 1.0 /ddot_(N, h, 1, y, 1);

                if(dfp_bfgs){
                    //BFGS formula
                    double k1 = k2*(1.0 + k2*ddot_(N, y, 1, v, 1));
                    dsyr_("U", N, k1, h, 1, D, N);
                    dsyr2_("U", N, -k2, h, 1, v, 1, D, N);
                }
                else{
                    //DFP Fuertes
			        double k1 = 1.0 / ddot_(N, y, 1, v, 1);
			        dsyr_("U", N, k2, h,1,D, N);
			        dsyr_("U", N, -k1, v,1,D,N);
                }
            }

		    // obtiene variables de criterios de paro
		    //dcopy_( N, xo, 1, temp, 1 );
		    //daxpy_( N, -1.0, x, 1, temp, 1 );
		    //normdX = dnrm2_(N,temp,1);	// diferencias entre puntos
		    //normX = dnrm2_(N,x,1);
		    //int pos = idamax_(N,g,1) - 1;
		    //normG = fabs( g[pos] ); // norma inf gradiente
            normG = dnrm2_(N, g, 1);
            crit = crite->check(N, k, xo, &Fo, x, &F, g);
    
            //Guarda secuencia
            file << k+1 <<" ";
	        for(int n = 0; n < N; n++)
                file << x[n] << " ";
            file << F << " " << normG << std::endl;

		    // respalda valores para prox. iteraccion, incrementa contador 
		    Fo = F;
		    dcopy_( N, x, 1, xo, 1 );	// posicion
		    k++;

		    // despliega resultados cada n-iteraciones
		    if(!Optim::quiet){
                if (k%100 == 0)
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }

	    }while(crit);
	
		Optim::iter_mem = k;
	    // free memory
	    file.close( );
        free_vectorr((void*) xo);
        free_vectorr((void*) x);
        free_vectorr((void*) y);
        free_vectorr((void*) g);
        free_vectorr((void*) go);
        free_vectorr((void*) h);
        free_vectorr((void*) v);
        free_vectorr((void*) D);
        free_vectorr((void*) temp);
    }

    /////////METODOS MINIMOS CUADRADOS NO LINEALES////////////


    //Metodo de Levenberg marquad
    static void LevenbergMarquad(VectorialFunction* func, 
                          double* xinit,
                          StopCriteria* crite,
                          std::ofstream& file,
                          bool numerical = false)
    {
        
        int N = func->dimIn();
        int M = func->dimOut();
        //posicion inicial
        //double xo[N] = {3.0,1.0};
        double* xo = (double*) vectorr(N, sizeof(double));
        dcopy_(N, xinit, 1, xo, 1);

        
  
        // declara variables de computo
        double* x = (double*) vectorr(N,sizeof(double));
        
        double* f = (double*) vectorr(M,sizeof(double));
        
        double* J = (double*) vectorr(N*M,sizeof(double));
        
        double* xw = (double*) vectorr(N,sizeof(double));
        
        double* fw = (double*) vectorr(M,sizeof(double));
        //std::cout <<"Here" << std::endl;
        double* Jw = (double*) vectorr(N*M,sizeof(double));
        double* h = (double*) vectorr(N,sizeof(double));  
        double* g = (double*) vectorr(N,sizeof(double));
        double* A = (double*) vectorr(N*N,sizeof(double));
        double* I = (double*) vectorr(N*N,sizeof(double));


        // parametros de criterios de paro
        double normG, tao = 1.0, epsilon1 = 1.0e-12, epsilon2 = 1.0e-12, mu, nu = 2.0;
        int pos, info, kmax = 3000, k = 0;
  

        // inicializa variables de computo
        dcopy_( N, xo, 1, x, 1 );       // posicion inicial    
        //Func( dummy, x, f, J );      // valores iniciales
        func->eval(x, f);
        func->jaco(x, J, numerical);

        dscal_( N*N, 0.0, I, 1 );        //matriz I 
        for ( int r = 0; r < N; r++)
            I[r*N+r] = 1.0;
        dsyrk_('U', 'T', N, M, 1.0, J, M, 0.0, A, N );       // matriz A = J'J  
        dgemv_( 'T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1 );    // g = J'f  
        pos = idamax_( N, g, 1 ) - 1;                                // norma INF de g
        normG = fabs(g[pos]);
        pos = idamax_( N, A, N ) ;                              // amortiguamiento inicial
        mu = tao*fabs(A[pos*N+pos]);
        

        // guarda secuencia
        file << "#  iter   |   x (dim:"<< N <<")  |   F(x)°F(x)  |   ||g||   |   mu" << std::endl;
        file <<"    " << k << "  ";
        for(int i = 0; i<N; i++){
            file << x[i] << "  ";
        }
        file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << mu << std::endl;

        // inicia iteraciones
        bool found = (normG <= epsilon1);
        while ( !found && (k < kmax) )
        {
            // obtiene paso Gauss-Newton
            dcopy_( N*N, I, 1, A, 1 );
            dsyrk_('U', 'T', N, M, 1.0, J, M, mu, A, N );        // matriz A = ( J'J + muI)
            dcopy_( N, g, 1, h, 1 );
            dscal_( N, -1.0, h, 1 );
            dposv_( 'U', N, 1, A, N, h, N, info );               // resuelve el sistema Ah = -g
            //std::cout << "Info: " << info << std::endl; 
      
            // prueba criterios de paro
            if ( dnrm2_(N,h,1) <= epsilon2*(dnrm2_(N,x,1)+epsilon2) )
                found = true;
            else
            {
                // estima nueva posicion y valores
                dcopy_( N, x, 1, xw, 1 );
                daxpy_( N, 1.0, h, 1, xw, 1 );
                //Func( dummy, xw, fw, Jw );
                func->eval(xw, fw);
                func->jaco(xw, Jw, numerical);
          
                // prueba si el paso es aceptable
                double ro = (ddot_(M,f,1,f,1) - ddot_(M,fw,1,fw,1)) / (mu*ddot_(N,h,1,h,1) - ddot_(N,h,1,g,1));
                if ( ro > 0.0 )
                {
                    // actualiza valores
                    dcopy_( N, xw, 1, x, 1 );
                    dcopy_( M, fw, 1, f, 1 );
                    dcopy_( M*N, Jw, 1, J, 1 );
                    dgemv_( 'T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1 );    // vector g = J'f      
              
                    // actualiza parametro amortiguamiento
                    double v1 = 1.0 / 3.0;
                    double v2 = 1.0 - pow(2.0*ro-1.0,3.0);
                    mu *= ((v1 > v2)  ?  v1 : v2);
                    nu = 2.0;
              
                    // obtiene variables de criterios de paro
                    pos = idamax_( N, g, 1 ) - 1;      // norma INF de g
                    normG = fabs(g[pos]);
                    found = (normG <= epsilon1);
                }
                else
                {
                    mu *= nu;
                    nu *= 2.0;            
                }  
            }  

            // incrementa contador, guarda secuencia
            k++;
            file <<"    " << k << "  ";
            for(int i = 0; i<N; i++){
                file << x[i] << "  ";
            }
            file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << mu << std::endl;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if ( k%10 == 0 )
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        }

        Optim::iter_mem = k;

        free_vectorr((void*)x);
        free_vectorr((void*)f);
        free_vectorr((void*)J);
        free_vectorr((void*)xw);
        free_vectorr((void*)fw);
        free_vectorr((void*)Jw);
        free_vectorr((void*)h);
        free_vectorr((void*)g);
        free_vectorr((void*)A);
        free_vectorr((void*)I);
        free_vectorr((void*)xo);
    }

    //metodo secante de levenberg marquad 
    static void LevenbergMarquadSecant(VectorialFunction* func, 
                                      double* xinit,
                                      StopCriteria* crite,
                                      std::ofstream& file)
    {
        int N = func->dimIn();
        int M = func->dimOut();

        double* xo = (double*) vectorr(N,sizeof(double));
        dcopy_(N, xinit, 1, xo, 1);
        // inicia algoritmo
        // declara variables de computo
        double* x = (double*) vectorr(N,sizeof(double));
        double* f = (double*) vectorr(M,sizeof(double));
        double* B = (double*) vectorr(M*N,sizeof(double));
        double* xw = (double*) vectorr(N,sizeof(double));
        double* fw = (double*) vectorr(M,sizeof(double));
        double* h = (double*) vectorr(N,sizeof(double));
        double* hw = (double*) vectorr(N,sizeof(double));  
        double* g = (double*) vectorr(N,sizeof(double));
        double* A = (double*) vectorr(N*N,sizeof(double));
        double* I = (double*) vectorr(N*N,sizeof(double));
        double* u = (double*) vectorr(M, sizeof(double));

        // parametros de criterios de paro
        double normG, tao = 1.0, epsilon1 = 1.0e-12, epsilon2 = 1.0e-12, mu, nu = 2.0;
        int pos, info, kmax = 3000, k = 0;


        // inicializa variables de computo
        dcopy_( N, xo, 1, x, 1 );       // posicion inicial    
        //Func( dummy, x, f );      // valores iniciales
        func->eval(x, f);
        dscal_( N*N, 0.0, I, 1 );        //matriz I 
        for ( int r = 0; r < M; r++)
            for(int c = 0; c < N; c++)
            {
                B[c*M+r] = 1.0;
                I[r*N+r] = 1.0;
            }
        
        dsyrk_('U', 'T', N, M, 1.0, B, M, 0.0, A, N );       // matriz A = B'B  
        
        dgemv_( 'T', M, N, 1.0, B, M, f, 1.0, 0.0, g, 1 );    // g = B'f  
        pos = idamax_( N, g, 1 ) - 1;                                // norma INF de g
        normG = fabs(g[pos]);
        pos = idamax_( N, A, N ) ;                              // amortiguamiento inicial
        mu = tao*fabs(A[pos*N+pos]);

        // guarda secuencia
        file << "#  iter   |   x (dim:"<< N <<")  |   F(x)°F(x)  |   ||g||   |   mu" << std::endl;
        file <<"    " << k << "  ";
        for(int i = 0; i<N; i++){
            file << x[i] << "  ";
        }
        file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << mu << std::endl;

        
        // inicia iteraciones
        bool found = (normG <= epsilon1);
        while ( !found && (k < kmax) )
        {
            // obtiene paso Gauss-Newton
            dcopy_( N*N, I, 1, A, 1 );
            dsyrk_('U', 'T', N, M, 1.0, B, M, mu, A, N );        // matriz A = ( B'B + muI)
            dcopy_( N, g, 1, h, 1 );
            dscal_( N, -1.0, h, 1 );
            dposv_( 'U', N, 1, A, N, h, N, info );                     // resuelve el sistema Ah = -g    
  
            // prueba criterios de paro
            if ( dnrm2_(N,h,1) <= epsilon2*(dnrm2_(N,x,1)+epsilon2) )
                found = true;
            else
            {
                //span in Rn
                int j = k%N;
                if( fabs(h[j]) < (0.8*dnrm2_(N,h,1)) )
                {
                    dcopy_(N,x,1,xw,1);
                    xw[j] += (1.0e-7)*(1.0+fabs(x[j]));
                    //Func(dummy, xw, fw);
                    func->eval(xw, fw);
                    dcopy_(N,xw,1,hw,1);
                    daxpy_(N,-1.0,x,1,hw,1);

                    //Broyden's rank-1 update
                    dgemv_('N',M,N,-1.0,B,M,hw,1,0.0,u,1); //u = -Bh
                    daxpy_(M,-1.0,f,1,u,1);
                    daxpy_(M,1.0,fw,1,u,1);
                    double cte = 1.0/ddot_(N,hw,1,hw,1);
                    dger_(M,N,cte,u,1,hw,1,B,M);
                }
 
                // estima nueva posicion y valores
                dcopy_( N, x, 1, xw, 1 );
                daxpy_( N, 1.0, h, 1, xw, 1 );
                //Func( dummy, xw, fw);
                func->eval(xw, fw);

                // update B using Broyden's rank-1 update
                dgemv_('N',M,N,-1.0,B,M,h,1,0.0,u,1); //u = -Bh
                daxpy_(M,-1.0,f,1,u,1);
                daxpy_(M,1.0,fw,1,u,1);
                double cte = 1.0/ddot_(N,h,1,h,1);
                dger_(M,N,cte,u,1,h,1,B,M);

                // prueba si el paso es aceptable
                double ro = (ddot_(M,f,1,f,1)-ddot_(M,fw,1,fw,1))/(mu*ddot_(N,h,1,h,1)-ddot_(N,h,1,g,1));
                if(ro>0.0)
                {
                    // Actualiza valores
                    dcopy_(N,xw,1,x,1);
                    dcopy_(M,fw,1,f,1);
 
                    // Actualiza parametro de amortiguamiento
                    double v1 = 1.0/3.0;
                    double v2 = 1.0 - pow(2.0*ro-1.0,3.0);
                    mu *= ((v1>v2)? v1 : v2);
                    nu = 2.0; 
                }
                else
                {
                    mu *= nu;
                    nu *= 2.0;
                }
       
                // calcula gradiente y criterios de paro
                dgemv_('T',M,N,1.0,B,M,f,1,0.0,g,1);
                pos = idamax_(N,g,1) - 1; // Norma INF de g
                normG = fabs(g[pos]);
                found = (normG <= epsilon1);
            }
            
            // incrementa contador, guarda secuencia
            k++;                                             // contador
            file <<"    " << k << "  ";
            for(int i = 0; i<N; i++){
                file << x[i] << "  ";
            }
            file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << mu << std::endl;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if ( k%10 == 0 )
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        }

        Optim::iter_mem = k;

        free_vectorr((void*)x);
        free_vectorr((void*)f);
        free_vectorr((void*)B);
        free_vectorr((void*)xw);
        free_vectorr((void*)fw);
        free_vectorr((void*)h);
        free_vectorr((void*)hw);
        free_vectorr((void*)g);
        free_vectorr((void*)A);
        free_vectorr((void*)I);
        free_vectorr((void*)u);
        free_vectorr((void*)xo);
    }

    //Metodo Dog Leg usando factorizacion QR
    static void DogLegQR(VectorialFunction* func, 
                double* xinit,
                StopCriteria* crite,
                std::ofstream& file,
                bool numerical = false)
    {

        int N = func->dimIn();
        int M = func->dimOut();
        double* xo = (double*) vectorr(N,sizeof(double));
        dcopy_(N, xinit, 1, xo, 1);
  
        // declara variables de computo
        double* x = (double*) vectorr(N,sizeof(double));
        double* f = (double*) vectorr(M,sizeof(double));
        double* J = (double*) vectorr(N*M,sizeof(double));
        double* xw = (double*) vectorr(N,sizeof(double));
        double* fw = (double*) vectorr(M,sizeof(double));
        double* Jw = (double*) vectorr(N*M,sizeof(double));
        double* h = (double*) vectorr(N,sizeof(double));  
        double* g = (double*) vectorr(N,sizeof(double));
        double* a = (double*) vectorr(N,sizeof(double));
        double* b = (double*) vectorr(M,sizeof(double));
        double* temp = (double*) vectorr(M,sizeof(double));
  
        // parametros de criterios de paro
        double epsilon1 = 1.0e-12, epsilon2 = 1.0e-12, epsilon3 = 1.0e-15;
        double normG, normFx, den, delta = 1.0;
        int pos, info, kmax = 3000, k = 0;
  
        // inicializa variables de computo
        dcopy_( N, xo, 1, x, 1 );       // posicion inicial    
        //Func( dummy, x, f, J );      // valores iniciales
        func->eval(x, f);
        func->jaco(x, J, numerical);
        dgemv_('T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1);
        pos = idamax_(N, g, 1) - 1;
        normG = fabs(g[pos]);
        pos = idamax_(N, f, 1) - 1;
        normFx = fabs(f[pos]);

        int lwork = -1;
        double wkopt;
        double* work = NULL;
        dgels_('N', M, N, 1, Jw, M, b, M, &wkopt, lwork, info);
        if(info > 0){
            std::cout << "\n \nError en la funcion LAPACK\n";
            return;
        }
        lwork = int(wkopt);
        work = (double*)vectorr(lwork, sizeof(double));

        // guarda secuencia
        file << "#  iter   |   x (dim:"<< N <<")  |   F(x)°F(x)  |   ||g||   |   delta" << std::endl;
        file <<"    " << k << "  ";
        for(int i = 0; i<N; i++){
            file << x[i] << "  ";
        }
        file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

        // inicia iteraciones
        bool found = (normFx <= epsilon3) || (normG <= epsilon1);
        while ( !found && (k < kmax) )
        {
            dgemv_('N', M, N, 1.0, J, M, g, 1, 0.0, temp, 1);
            double alfa = ddot_(N, g, 1,g, 1) / ddot_(M, temp, 1, temp, 1);
            dcopy_(N, g, 1, a, 1);
            dscal_(N, -alfa, a, 1);


            dcopy_(M, f, 1, b, 1);
            dscal_(M, -1.0, b, 1);
            dcopy_(M*N, J, 1, Jw, 1);
            dgels_('N', M, N, 1, Jw, M, b, M, work, lwork, info);

            if(dnrm2_(N, b, 1) <= delta){
                dcopy_(N, b, 1, h, 1);
                den = 0.5*ddot_(M, f, 1, f, 1);
            }
            else if(dnrm2_(N, a, 1) >= delta){
                dcopy_(N, g, 1, h, 1);
                double valor = delta / dnrm2_(N, g, 1);
                dscal_(N, -valor, h, 1);
                den = delta*(2.0*dnrm2_(N, a, 1) - delta)/(2.0*alfa);
            }
            else{
                //PTHERE IS A BUG HERE
                dcopy_(N, b, 1, temp, 1);
                daxpy_(N, -1.0, a, 1, temp, 1);
                double c = ddot_(N, a, 1, temp, 1);
                double valor = sqrt(pow(c, 2.0) + ddot_(N, temp, 1, temp, 1)*(pow(delta, 2.0) - ddot_(N, a, 1, a, 1)));
                double beta = 0.0;
                if(c <= 0){
                    beta = (-c + valor)/ddot_(N, temp, 1, temp, 1);
                }
                else{
                    beta = (pow(delta, 2.0) - ddot_(N, a, 1, a, 1))/(c+valor);
                }
                dcopy_(N, a, 1, h, 1);
                daxpy_(N, beta, temp, 1, h, 1);
                den = 0.5*alfa*pow(1.0 - beta, 2.0)*ddot_(N, g, 1, g, 1) + beta*(2.0 - beta)*0.5*ddot_(M, f, 1, f, 1);
            }

            if(dnrm2_(N, h, 1) <= epsilon2*(dnrm2_(N,x,1)+epsilon2)){
                found = true;
            }
            else{
                dcopy_(N, x, 1, xw, 1);
                daxpy_(N, 1.0, h, 1, xw, 1);
                //Func(dummy, xw, fw, Jw);
                func->eval(xw, fw);
                func->jaco(xw, Jw, numerical);
                double ro = 0.5*(ddot_(M, f, 1, f, 1) - ddot_(M, fw, 1, fw, 1))/den;

                if(ro > 0.0){
                    dcopy_(N, xw, 1, x, 1);
                    dcopy_(M, fw, 1, f, 1);
                    dcopy_(M*N, Jw, 1, J, 1);
                    dgemv_('T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1);

                    pos = idamax_(N, g, 1) - 1;
                    normG = fabs(g[pos]);
                    pos = idamax_(N, f, 1) - 1;
                    normFx = fabs(f[pos]);
                    found = (normFx <= epsilon3) || (normG <= epsilon1);
                }
                if(ro > 0.75){
                    delta = (delta > 3.0*dnrm2_(N, h, 1)) ? delta : 3.0*dnrm2_(N, h, 1);
                }
                else if(ro < 0.25){
                    delta *= 0.5;
                    found = (delta <= epsilon2*(dnrm2_(N, x, 1) + epsilon2));
                }
            }

            // incrementa contador, guarda secuencia
            k++;                                             // contador
            file <<"    " << k << "  ";
            for(int i = 0; i<N; i++){
                file << x[i] << "  ";
            }
            file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if ( k%10 == 0 )
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        }

        Optim::iter_mem = k;

        free_vectorr((void*)x);
        free_vectorr((void*)f);
        free_vectorr((void*)J);
        free_vectorr((void*)xw);
        free_vectorr((void*)fw);
        free_vectorr((void*)Jw);
        free_vectorr((void*)h);
        free_vectorr((void*)g);
        free_vectorr((void*)a);
        free_vectorr((void*)b);
        free_vectorr((void*)temp);
        free_vectorr((void*)xo);
        free_vectorr((void*)work);
    }

    static void DogLegSecant(VectorialFunction* func,
                             double* xinit,
                             StopCriteria* crit,
                             std::ofstream& file)
    {
        
        int N = func->dimIn(), M = func->dimOut();
        double* xo = (double*)vectorr(N, sizeof(double)); 
        dcopy_(N, xinit, 1, xo, 1);

        // declara variables de computo
        double* x = (double*) vectorr(N,sizeof(double));
        double* f = (double*) vectorr(M,sizeof(double));
        double* B = (double*) vectorr(N*M,sizeof(double));
        double* D = (double*) vectorr(N*M,sizeof(double));
        double* xw = (double*) vectorr(N,sizeof(double));
        double* fw = (double*) vectorr(M,sizeof(double));
        double* h = (double*) vectorr(N,sizeof(double));
        double* hw = (double*) vectorr(N,sizeof(double)); 
        double* g = (double*) vectorr(N,sizeof(double));
        double* a = (double*) vectorr(N,sizeof(double));
        double* b = (double*) vectorr(N,sizeof(double));
        double* u = (double*) vectorr(M,sizeof(double));
        double* y = (double*) vectorr(M,sizeof(double));  

  
        // parametros de criterios de paro
        double epsilon1 = 1.0e-12, epsilon2 = 1.0e-12, epsilon3 = 1.0e-15;
        double normG, normFx, den, delta = 1.0;
        int pos, kmax = 3000, k = 0;
  

        // inicializa variables de computo
        dcopy_( N, xo, 1, x, 1 );       // posicion inicial    
        //Func( dummy, x, f );      // valores iniciales
        func->eval(x, f);
        for ( int r = 0; r < M; r++)
            for ( int c = 0; c < N; c++)
            {
                B[c*M+r] = 1.0;
                D[c*N+r] = 1.0;
            }
        dgemv_( 'T', M, N, 1.0, B, M, f, 1.0, 0.0, g, 1 );    // g = B'f  
        pos = idamax_( N, g, 1 ) - 1;                                // norma INF de g
        normG = fabs(g[pos]);
        pos = idamax_( N, f, 1 ) - 1;                                // norma INF de f
        normFx = fabs(f[pos]);

        // calcula espacio de trabajo para la funcion SVD
        double* S = (double*) vectorr(N,sizeof(double));
        double* U = (double*) vectorr(M*M,sizeof(double));
        double* V = (double*) vectorr(N*N,sizeof(double));
        int info, lwork = -1;
        double wkopt, *work;
        dgesvd_( 'A', 'A', M, N, B, M, S, U, M, V, N, &wkopt, lwork, info );
        lwork = int(wkopt);
        work = (double*) vectorr( lwork,sizeof(double));


        // guarda secuencia
        file << "#  iter   |   x (dim:"<< N <<")  |   F(x)°F(x)  |   ||g||   |   delta" << std::endl;
        file <<"    " << k << "  ";
        for(int i = 0; i<N; i++){
            file << x[i] << "  ";
        }
        file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

        // inicia iteraciones
        bool found = (normFx <= epsilon3) || (normG <= epsilon1);
        while ( !found && (k < kmax) )
        {
            // obtiene paso descenso gradiente
            dgemv_( 'N', M, N, 1.0, B, M, g, 1, 0.0, u, 1 );
            double alfa = ddot_( N, g, 1, g, 1 ) / ddot_( M, u, 1, u, 1 );
            dcopy_( N, g, 1, a, 1 );
            dscal_( N, -alfa, a, 1 );
      
            // obtiene paso Gauss-Newton
            dgemv_( 'N', N, M, -1.0, D, N, f, 1.0, 0.0, b, 1 );    // b = -Df  


            // estima paso optimo del algoritmo usando estrategia de POWELL
            if ( dnrm2_(N,b,1) <= delta )
            {
                dcopy_( N, b, 1, h, 1 );
                den = 0.5*ddot_( M, f, 1, f, 1 );
            }
            else if ( dnrm2_(N,a,1) >= delta )
            {
                dcopy_( N, g, 1, h, 1 );
                double valor = delta / dnrm2_( N, g, 1 );
                dscal_( N, -valor, h, 1 );
                den = delta*(2.0*dnrm2_( N, a, 1 ) - delta) / (2.0*alfa);
            }
            else
            {
                dcopy_( N, b, 1, u, 1 );
                daxpy_( N, -1.0, a, 1, u, 1 );
                double c = ddot_( N, a, 1, u, 1 );
                double valor = sqrt( pow(c,2.0) + ddot_(N,u,1,u,1)*(pow(delta,2.0) - ddot_(N,a,1,a,1)) );
                double beta = 0.0;
                if ( c <= 0 )
                    beta = (-c + valor) / ddot_(N,u,1,u,1);
                else
                    beta = ( pow(delta,2.0) - ddot_(N,a,1,a,1) ) / (c + valor);
                dcopy_( N, a, 1, h, 1 );
                daxpy_( N, beta, u, 1, h, 1 );
                den = 0.5*alfa*pow(1.0-beta,2.0)*ddot_(N,g,1,g,1) + beta*(2.0-beta)*0.5*ddot_(M,f,1,f,1);
            }

            // prueba criterios de paro
            if ( dnrm2_(N,h,1) <= epsilon2*(dnrm2_(N,x,1)+epsilon2) )
                found = true;
            else
            {
                // span in Rn
                int j = k%N;
                if ( fabs(h[j]) < (0.8*dnrm2_(N,h,1)) )
                {
                    dcopy_( N, x, 1, xw, 1 );
                    xw[j] += (1.0e-7)*(1.0 + fabs(x[j]));
                    //Func( dummy, xw, fw );
                    func->eval(xw, fw);
                    dcopy_( N, xw, 1, hw, 1 );
                    daxpy_( N, -1.0, x, 1, hw, 1 );
              
                    // Broyden rank-1 update
                    dgemv_( 'N', M, N, -1.0, B, M, hw, 1, 0.0, u, 1 );    // u = -Bh  
                    daxpy_( M, -1.0, f, 1, u, 1 );     
                    daxpy_( M, 1.0, fw, 1, u, 1 );
                    double cte = 1.0 / ddot_( N, hw, 1, hw, 1 );
                    dger_( M, N, cte, u, 1, hw, 1, B, M );
                }
          
                // estima nueva posicion y valores
                dcopy_( N, x, 1, xw, 1 );
                daxpy_( N, 1.0, h, 1, xw, 1 );
                //Func( dummy, xw, fw );
                func->eval(xw, fw);

                // update B using Broyden rank-1 update
                dgemv_( 'N', M, N, -1.0, B, M, h, 1, 0.0, u, 1 );    // u = -Bh  
                daxpy_( M, -1.0, f, 1, u, 1 );     
                daxpy_( M, 1.0, fw, 1, u, 1 );
                double cte = 1.0 / ddot_( N, h, 1, h, 1 );
                dger_( M, N, cte, u, 1, h, 1, B, M );

                // update D using Broyden rank-1 update
                dcopy_( N, fw, 1, y, 1 );
                daxpy_( N, -1.0, f, 1, y, 1 );
                dgemv_( 'N', N, M, 1.0, D, N, y, 1, 0.0, u, 1 );    // u = Dy 
                cte = ddot_( N, h, 1, u, 1 );
                if ( cte < sqrt(DBL_EPSILON)*dnrm2_(N,h,1) )
                {
                    // then D is not updated, but computed as D = B^âˆ’1
                    dgesvd_( 'S', 'S', M, N, B, M, S, U, M, V, N, work, lwork, info );
                    for ( int r = 0; r < N; r++ )
                    {
                        double val = 0.0;
                        if ( S[r] > sqrt(DBL_EPSILON) )
                            val = 1.0 / S[r];
                        dscal_( M, val, &U[r*M], 1 );
                    }
                    dgemm_( 'T', 'T', N, M, N, 1.0, V, N, U, M, 0.0, D, N );
                }
                else
                {
                    dscal_( N, -1.0, u, 1 );
                    daxpy_( N, 1.0, h, 1, u, 1 );
                    dgemm_( 'N', 'N', 1, M, N, 1.0, h, 1, D, N, 0.0, y, 1 );    // y = h'D  
                    dger_( N, M, (1.0/cte), u, 1, y, 1, D, N );
                }

                // prueba si el paso es aceptable
                double ro = 0.5*(ddot_(M,f,1,f,1) - ddot_(M,fw,1,fw,1)) / den;
                if ( ro > 0.0 )
                {
                // actualiza valores
                    dcopy_( N, xw, 1, x, 1 );
                    dcopy_( M, fw, 1, f, 1 );
                    pos = idamax_( N, f, 1 ) - 1;      // norma INF de f
                    normFx = fabs(f[pos]);
                    found = (normFx <= epsilon3);
                }
                if (ro > 0.75)
                {
                    delta = (delta > 3.0*dnrm2_(N,h,1)) ? delta : 3.0*dnrm2_(N,h,1);        
                }     
                else if (ro < 0.25)
                {
                    delta *= 0.5;
                    found = (delta <= epsilon2*(dnrm2_(N,x,1)+epsilon2) );
                }

                // calcula gradiente y variables de criterios de paro
                dgemv_( 'T', M, N, 1.0, B, M, f, 1.0, 0.0, g, 1 );    // vector g = J'f      
                pos = idamax_( N, g, 1 ) - 1;      // norma INF de g
                normG = fabs(g[pos]);
                found = found || (normG <= epsilon1);
            }  

            // incrementa contador, guarda secuencia
            k++;                                             // contador
            file <<"    " << k << "  ";
            for(int i = 0; i<N; i++){
                file << x[i] << "  ";
            }
            file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if ( k%10 == 0 )
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        }

        Optim::iter_mem = k;

        // declara variables de computo
        free_vectorr((void*)x);
        free_vectorr((void*)f);
        free_vectorr((void*)B);
        free_vectorr((void*)D);
        free_vectorr((void*)xw);
        free_vectorr((void*)fw);
        free_vectorr((void*)h);
        free_vectorr((void*)hw);
        free_vectorr((void*)g);
        free_vectorr((void*)a);
        free_vectorr((void*)b);
        free_vectorr((void*)u);
        free_vectorr((void*)y);
        free_vectorr((void*)xo);
        free_vectorr((void*)S);
        free_vectorr((void*)U);
        free_vectorr((void*)V);
        //std::cout << "BWork dir:" << bwork << std::endl;
        free_vectorr((void*)work);
    }

    static void DogLegSVD(VectorialFunction* func,
                          double* xinit,
                          StopCriteria* crit,
                          std::ofstream&  file,
                          bool numerical = false)
    {
        int N = func->dimIn();
        int M = func->dimOut();
        double* xo = (double*) vectorr(N,sizeof(double));
        dcopy_(N, xinit, 1, xo, 1);

        // declara variables de computo
        double* x = (double*) vectorr(N,sizeof(double));
        double* f = (double*) vectorr(M,sizeof(double));
        double* J = (double*) vectorr(N*M,sizeof(double));
        double* xw = (double*) vectorr(N,sizeof(double));
        double* fw = (double*) vectorr(M,sizeof(double));
        double* Jw = (double*) vectorr(N*M,sizeof(double));
        double* h = (double*) vectorr(N,sizeof(double));  
        double* g = (double*) vectorr(N,sizeof(double));
        double* a = (double*) vectorr(N,sizeof(double));
        double* b = (double*) vectorr(M,sizeof(double));
        double* temp = (double*) vectorr(M,sizeof(double));

  
        // parametros de criterios de paro
        double epsilon1 = 1.0e-12, epsilon2 = 1.0e-12, epsilon3 = 1.0e-15;
        double normG, normFx, den, delta = 1.0;
        int pos, info, kmax = 3000, k = 0;
  

        // inicializa variables de computo
        dcopy_( N, xo, 1, x, 1 );       // posicion inicial    
        //Func( dummy, x, f, J );      // valores iniciales
        func->eval(x, f);
        func->jaco(x, J, numerical);
        dgemv_('T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1);
        pos = idamax_(N, g, 1) - 1;
        normG = fabs(g[pos]);
        pos = idamax_(N, f, 1) - 1;
        normFx = fabs(f[pos]);

        //calcula el espacio de trabajo para la funcion SVD
        int rank, lwork = -1;
        double rcond, wkopt;
        double* work;
        dgelss_(M, N, 1, Jw, M, b, M, temp, rcond, rank, &wkopt, lwork, info);
        if(info > 0){
            std::cout << "\n \nError en la funcion LAPACK\n";
            return;
        }
        lwork = int(wkopt);
        work = (double*)vectorr(lwork, sizeof(double));

        // guarda secuencia
        file << "#  iter   |   x (dim:"<< N <<")  |   F(x)°F(x)  |   ||g||   |   delta" << std::endl;
        file <<"    " << k << "  ";
        for(int i = 0; i<N; i++){
            file << x[i] << "  ";
        }
        file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

        // inicia iteraciones
        bool found = (normFx <= epsilon3) || (normG <= epsilon1);
        while ( !found && (k < kmax) )
        {
            dgemv_('N', M, N, 1.0, J, M, g, 1, 0.0, temp, 1);
            double alfa = ddot_(N, g, 1,g, 1) / ddot_(M, temp, 1, temp, 1);
            dcopy_(N, g, 1, a, 1);
            dscal_(N, -alfa, a, 1);

            //obtiene paso de gauss newton
            dcopy_(M, f, 1, b, 1);
            dscal_(M, -1.0, b, 1);
            dcopy_(M*N, J, 1, Jw, 1);
            dgelss_(M, N, 1, Jw, M, b, M, temp, rcond, rank, work, lwork, info);

            if(dnrm2_(N, b, 1) <= delta){
                dcopy_(N, b, 1, h, 1);
                den = 0.5*ddot_(M, f, 1, f, 1);
            }
            else if(dnrm2_(N, a, 1) >= delta){
                dcopy_(N, g, 1, h, 1);
                double valor = delta / dnrm2_(N, g, 1);
                dscal_(N, -valor, h, 1);
                den = delta*(2.0*dnrm2_(N, a, 1) - delta)/(2.0*alfa);
            }
            else{
                dcopy_(N, b, 1, temp, 1);
                daxpy_(N, -1.0, a, 1, temp, 1);
                double c = ddot_(N, a, 1, temp, 1);
                double valor = sqrt(pow(c, 2.0) + ddot_(N, temp, 1, temp, 1)*(pow(delta, 2.0) - ddot_(N, a, 1, a, 1)));
                double beta = 0.0;
                if(c <= 0){
                    beta = (-c + valor)/ddot_(N, temp, 1, temp, 1);
                }
                else{
                    beta = (pow(delta, 2.0) - ddot_(N, a, 1, a, 1))/(c+valor);
                }
                dcopy_(N, a, 1, h, 1);
                daxpy_(N, beta, temp, 1, h, 1);
                den = 0.5*alfa*pow(1.0 - beta, 2.0)*ddot_(N, g, 1, g, 1) + beta*(2.0 - beta)*0.5*ddot_(M, f, 1, f, 1);
            }

            if(dnrm2_(N, h, 1) <= epsilon2*(dnrm2_(N,x,1)+epsilon2)){
                found = true;
            }
            else{
                dcopy_(N, x, 1, xw, 1);
                daxpy_(N, 1.0, h, 1, xw, 1);
                //Func(dummy, xw, fw, Jw);
                func->eval(xw, fw);
                func->jaco(xw, Jw, numerical);
                double ro = 0.5*(ddot_(M, f, 1, f, 1) - ddot_(M, fw, 1, fw, 1))/den;

                if(ro > 0.0){
                    dcopy_(N, xw, 1, x, 1);
                    dcopy_(M, fw, 1, f, 1);
                    dcopy_(M*N, Jw, 1, J, 1);
                    dgemv_('T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1);

                    pos = idamax_(N, g, 1) - 1;
                    normG = fabs(g[pos]);
                    pos = idamax_(N, f, 1) - 1;
                    normFx = fabs(f[pos]);
                    found = (normFx <= epsilon3) || (normG <= epsilon1);
                }
                if(ro > 0.75){
                    delta = (delta > 3.0*dnrm2_(N, h, 1)) ? delta : 3.0*dnrm2_(N, h, 1);
                }
                else if(ro < 0.25){
                    delta *= 0.5;
                    found = (delta <= epsilon2*(dnrm2_(N, x, 1) + epsilon2));
                }
            }

            // incrementa contador, guarda secuencia
            k++;                                             // contador
            file <<"    " << k << "  ";
            for(int i = 0; i<N; i++){
                file << x[i] << "  ";
            }
            file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if ( k%10 == 0 )
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        }

        Optim::iter_mem = k;

        free_vectorr((void*)x);
        free_vectorr((void*)f);
        free_vectorr((void*)J);
        free_vectorr((void*)xw);
        free_vectorr((void*)fw);
        free_vectorr((void*)Jw);
        free_vectorr((void*)h);
        free_vectorr((void*)g);
        free_vectorr((void*)a);
        free_vectorr((void*)b);
        free_vectorr((void*)temp);
        free_vectorr((void*)xo);
        free_vectorr((void*)work); //LIBERAR WORK ARROJA VIOLACION DE SEGMENTO
    }

    //Dog Leg con descomposicion LU
    static void DogLegLU(VectorialFunction* func,
                          double* xinit,
                          StopCriteria* crit,
                          std::ofstream&  file,
                          bool numerical = false)
    {
        
        int N = func->dimIn(); 
        int M = func->dimOut();
        double* xo = (double*) vectorr(N,sizeof(double));
        dcopy_(N, xinit, 1, xo, 1);

  
        // declara variables de computo
        double* x = (double*) vectorr(N,sizeof(double));
        double* f = (double*) vectorr(M,sizeof(double));
        double* J = (double*) vectorr(N*M,sizeof(double));
        double* xw = (double*) vectorr(N,sizeof(double));
        double* fw = (double*) vectorr(M,sizeof(double));
        double* Jw = (double*) vectorr(N*M,sizeof(double));
        double* h = (double*) vectorr(N,sizeof(double));  
        double* g = (double*) vectorr(N,sizeof(double));
        double* a = (double*) vectorr(N,sizeof(double));
        double* b = (double*) vectorr(N,sizeof(double));
        double* temp = (double*) vectorr(M,sizeof(double));
        double* A = (double*) vectorr(N*N,sizeof(double));
        int* ipiv = (int*)vectorr(N,sizeof(int));

  
        // parametros de criterios de paro
        double epsilon1 = 1.0e-12, epsilon2 = 1.0e-12, epsilon3 = 1.0e-15;
        double normG, normFx, den, delta = 1.0;
        int pos, info, kmax = 3000, k = 0;
  
        // inicializa variables de computo
        dcopy_( N, xo, 1, x, 1 );      // posicion inicial    
        //Func( dummy, x, f, J );     // valores iniciales
        func->eval(x, f);
        func->jaco(x, J, numerical);
        dgemv_( 'T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1 );    // g = J'f  
        pos = idamax_( N, g, 1 ) - 1;                               // norma INF de g
        normG = fabs(g[pos]);
        pos = idamax_( N, f, 1 ) - 1;                               // norma INF de f
        normFx = fabs(f[pos]);

        /*
        // calcula espacio de trabajo para la funcion QR
        int lwork = -1, rank;
        double wkopt, *work, rcond;
        //dgelss_( 'N', M, N, 1, Jw, M, b, N, &wkopt, lwork, info );
        dgelss_(M,N,1,Jw,M,b,N,temp,rcond, rank, &wkopt, lwork, info);
        if ( info > 0 )
          {
            cout << "\n\nError en funcion LAPACK....!!!\n";
            return EXIT_FAILURE;
          } 
        lwork = int(wkopt);
        work = (double*) vectorr( lwork,sizeof(double));
        */

        // guarda secuencia
        file << "#  iter   |   x (dim:"<< N <<")  |   F(x)°F(x)  |   ||g||   |   delta" << std::endl;
        file <<"    " << k << "  ";
        for(int i = 0; i<N; i++){
            file << x[i] << "  ";
        }
        file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

        // inicia iteraciones
        bool found = (normFx <= epsilon3) || (normG <= epsilon1);
        while ( !found && (k < kmax) )
        {
            // obtiene paso descenso gradiente
            dgemv_( 'N', M, N, 1.0, J, M, g, 1, 0.0, temp, 1 );
            double alfa = ddot_( N, g, 1, g, 1 ) / ddot_( M, temp, 1, temp, 1 );
            dcopy_( N, g, 1, a, 1 );
            dscal_( N, -alfa, a, 1 );
      
             // obtiene paso Gauss-Newton
            dgemv_('T',M,N,-1.0, J,M,f,1.0,0.0,b,1);
            dgemm_('T','N',N,N,M,1.0,J,M,J,M,0.0,A,N);
            dgesv_(N,1,A,N,ipiv,b,N,info);

            // estima paso optimo del algoritmo usando estrategia de POWELL
            if ( dnrm2_(N,b,1) <= delta )
            {
                dcopy_( N, b, 1, h, 1 );
                den = 0.5*ddot_( M, f, 1, f, 1 );
            }
            else if ( dnrm2_(N,a,1) >= delta )
            {
                dcopy_( N, g, 1, h, 1 );
                double valor = delta / dnrm2_( N, g, 1 );
                dscal_( N, -valor, h, 1 );
                den = delta*(2.0*dnrm2_( N, a, 1 ) - delta) / (2.0*alfa);
            }   
            else
            {
                dcopy_( N, b, 1, temp, 1 );
                daxpy_( N, -1.0, a, 1, temp, 1 );
                double c = ddot_( N, a, 1, temp, 1 );
                double valor = sqrt( pow(c,2.0) + ddot_(N,temp,1,temp,1)*(pow(delta,2.0) 
                                                           - ddot_(N,a,1,a,1)) );
                double beta = 0.0;
                if ( c <= 0 )
                    beta = (-c + valor) / ddot_(N,temp,1,temp,1);
                else
                    beta = ( pow(delta,2.0) - ddot_(N,a,1,a,1) ) / (c + valor);
                dcopy_( N, a, 1, h, 1 );
                daxpy_( N, beta, temp, 1, h, 1 );
                den = 0.5*alfa*pow(1.0-beta,2.0)*ddot_(N,g,1,g,1) 
                    + beta*(2.0-beta)*0.5*ddot_(M,f,1,f,1);
            }

            // prueba criterios de paro
            if ( dnrm2_(N,h,1) <= epsilon2*(dnrm2_(N,x,1)+epsilon2) )
                found = true;
            else
            {
                // estima nueva posicion y valores
                dcopy_( N, x, 1, xw, 1 );
                daxpy_( N, 1.0, h, 1, xw, 1 );
                //Func( dummy, xw, fw, Jw );
                func->eval(xw, fw);
                func->jaco(xw, Jw, numerical);
                double ro = 0.5*(ddot_(M,f,1,f,1) - ddot_(M,fw,1,fw,1)) / den;
          
                // prueba si el paso es aceptable
                if ( ro > 0.0 )
                {
                    // actualiza valores
                    dcopy_( N, xw, 1, x, 1 );
                    dcopy_( M, fw, 1, f, 1 );
                    dcopy_( M*N, Jw, 1, J, 1 );
                    dgemv_( 'T', M, N, 1.0, J, M, f, 1.0, 0.0, g, 1 );    // vector g = J'f     

                    // obtiene variables de criterios de paro
                    pos = idamax_( N, g, 1 ) - 1;     // norma INF de g
                    normG = fabs(g[pos]);
                    pos = idamax_( N, f, 1 ) - 1;     // norma INF de f
                    normFx = fabs(f[pos]);
                    found = (normFx <= epsilon3) || (normG <= epsilon1);
                }
                if (ro > 0.75)
                {
                    delta = (delta > 3.0*dnrm2_(N,h,1)) ? delta : 3.0*dnrm2_(N,h,1);        
                } 
                else if (ro < 0.25)
                {
                    delta *= 0.5;
                    found = (delta <= epsilon2*(dnrm2_(N,x,1)+epsilon2) );
                } 
            } 

            // incrementa contador, guarda secuencia
            k++;                                         
            file <<"    " << k << "  ";
            for(int i = 0; i<N; i++){
                file << x[i] << "  ";
            }
            file << (0.5*ddot_(M,f,1,f,1)) << "  " << normG << "  " << delta << std::endl;

            // despliega resultados cada n-iteraciones
            if(!Optim::quiet){
                if ( k%10 == 0 )
                    std::cout << "iter: " << k << std::scientific << " norm: " << normG << std::endl;
            }
        }

        Optim::iter_mem = k;

        free_vectorr((void*)x);
        free_vectorr((void*)f);
        free_vectorr((void*)J);
        free_vectorr((void*)xw);
        free_vectorr((void*)fw);
        free_vectorr((void*)Jw);
        free_vectorr((void*)h);
        free_vectorr((void*)g);
        free_vectorr((void*)a);
        free_vectorr((void*)b);
        free_vectorr((void*)temp);
        free_vectorr((void*)A);
        free_vectorr((void*)ipiv);
        free_vectorr((void*)xo);
    }
};

bool Optim::quiet = false;
int Optim::iter_mem = 0;

#endif