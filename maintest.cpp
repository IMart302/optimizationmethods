#include <cmath>
#include <cfloat>
#include <iostream>
#include <ctime>
#include <fstream>
#include <string>
#include <vector>

#include "optimi.h"
#include "function.h"

void test_all(FunctionND* func, double* xinit, std::string prefix_name, bool quiet = false);
void test_all2(VectorialFunction* func, double* xinit, std::string prefix_name, bool quiet = false);
double cputime2ms(timespec& inicio, timespec& fin);
void print_alg_data(double time, int calls, int iters);
void save_alg_data(double ftime, int calls, int iters, std::vector<double>& extime, std::vector<int>& fcalls, std::vector<int>& siters);
void print_alg_brief(std::vector<double>& extime, std::vector<int>& fcalls, std::vector<int>& siters);
void test_function(FunctionND* func);

int main(){
    VectorialFunction* func1 = new RosenbrockVec();
    double* xinit1 = func1->reserveIn();
    xinit1[0] = -1.2; xinit1[1] = 1.0;

    VectorialFunction* func2 = new VecExample32();
    double* xinit2 = func2->reserveIn();
    xinit2[0] = 3.0; xinit2[1] = 1.0;

    VectorialFunction* func3 = new ExponentialFit();
    double* xinit3 = func3->reserveIn();
    xinit3[0] = -1.0; xinit3[1] = -2.0;
    xinit3[2] = 1.0; xinit3[3] = -1.0;

    VectorialFunction* func4 = new VecExample31();
    double* xinit4 = func4->reserveIn();
    xinit4[0] = -1.2; xinit4[1] = 1.0;

    std::cout << "\n########### EJECUTANDO TEST ROSENBROCK #############\n \n";

    test_all2(func1, xinit1, "rosenv_", true);

    std::cout << "\n############ EJECUTANDO EJEMPLO 3.2 ###################\n \n";

    test_all2(func2, xinit2, "example32_", true);

    //std::cout << "\nEJECUTANDO TEST EJEMPLO 3.1\n \n";
//
    //test_all2(func4, xinit4,"exaple31_", true);

    std::cout << "\n############ EJECUTANDO TEST EXPONENTIAL FIT ###########\n \n";

    test_all2(func3, xinit3, "expfit_", true);

    

    delete func1; delete func2; delete func3;
    delete xinit1; delete xinit2; delete xinit3;

    return 0;
}


void test_all(FunctionND* func, double* xinit, std::string prefix_name, bool quiet){
    std::vector<int> itermem;
    std::vector<double> extime;
    std::vector<int> fcalls;
    
    StopCriteria* criteria1 = new StopCriteria();
    criteria1->reserveMem(func->dimIn());
    criteria1->epsilon1 = 1.0e-8;
    criteria1->epsilon2 = 1.0e-10;
    criteria1->epsilon3 = 1.0e-10;
    criteria1->crit = 0;

    StopCriteria* criteria2 = new StopCriteria();
    criteria2->reserveMem(func->dimIn());
    criteria2->epsilon1 = 1.0e-8;
    criteria2->epsilon2 = 1.0e-10;
    criteria2->epsilon3 = 1.0e-10;
    criteria2->crit = 1;

    Optim::quiet = quiet;

    LineSearch* noline = new NoneLineSearch();
    noline->alfa_default = 0.001;
    LineSearch* softline = new SoftLineSearch();
    LineSearch* exacline = new ExactLineSearch();
    LineSearch* interline = new InterLineSearch();
    LineSearch* btline = new BacktrackLineSearch();

    std::ofstream filelog;

    timespec inicio, fin;
    
    //DESCENSO DEL GRADIENTE SIN BUSQUEDA DE LINEA (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba Descenso del gradiente sin busqueda de linea\n \n";
    filelog.open((prefix_name+std::string("grandient_descend_nolinesearch_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::GradientDescend(func, xinit, noline, criteria1, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //DESCENSO DEL GRADIENTE CON INTERPOLIACION (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba Descenso del gradiente con interpolacion\n \n";
    filelog.open((prefix_name+std::string("grandient_descend_interpol_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::GradientDescend(func, xinit, interline, criteria1, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //DESCENSO DEL GRADIENTE CON BACKTRACKING LINESEARCH (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba Descenso del gradiente con backtracking\n \n";
    filelog.open((prefix_name+std::string("grandient_descend_backtrack_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::GradientDescend(func, xinit, btline, criteria1, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //DESCENSO DEL GRADIENTE CON SOFTLINE SEARCH (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba Descenso del gradiente con softline search\n \n";
    filelog.open((prefix_name+std::string("grandient_descend_softline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::GradientDescend(func, xinit, softline, criteria1, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //DESCENSO DEL GRADIENTE CON SOFTLINE SEARCH (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba Descenso del gradiente con softline search (numerico) \n \n";
    filelog.open((prefix_name+std::string("grandient_descend_softline_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::GradientDescend(func, xinit, softline, criteria1, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //DESCENSO DEL GRADIENTE CON EXACTLINE SEARCH (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba Descenso del gradiente con exactline search\n \n";
    filelog.open((prefix_name+std::string("grandient_descend_exactline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::GradientDescend(func, xinit, exacline, criteria1, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //DESCENSO DEL GRADIENTE CON EXACTLINE SEARCH (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba Descenso del gradiente con exactline search (numerico)\n \n";
    filelog.open((prefix_name+std::string("grandient_descend_exactline_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::GradientDescend(func, xinit, exacline, criteria1, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //GRADIENTE CONJUGADO POLAK RIBIERE CON SOFTLINE SEARCH (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba del gradiente conjugado tipo polak-ribiere con softline search\n \n";
    filelog.open((prefix_name+std::string("conj_grad_pr_softline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::ConjugateGradient(func, xinit, softline, criteria2, filelog, false);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //GRADIENTE CONJUGADO CON POLAK RIBIERE CON EXACTLINE SEARCH (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba del gradiente conjugado tipo polak-ribiere con exactline search\n \n";
    filelog.open((prefix_name+std::string("conj_grad_pr_exactline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::ConjugateGradient(func, xinit, exacline, criteria2, filelog, false);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //GRADIENTE CONJUGADO CON POLAK RIBIERE CON SOFTLINE SEARCH (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba del gradiente conjugado tipo polak-ribiere con softline search (numerico)\n \n";
    filelog.open((prefix_name+std::string("conj_grad_pr_softline_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::ConjugateGradient(func, xinit, softline, criteria2, filelog, false, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem); 

    //GRADIENTE CONJUGADO CON FLETCHER REEVES CON SOFTLINE SEARCH (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba del gradiente conjugado tipo fletcher-reeves con softline search\n \n";
    filelog.open((prefix_name+std::string("conj_grad_fr_softline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::ConjugateGradient(func, xinit, softline, criteria2, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //GRADIENTE CONJUGADO CON FLETCHER REEVES CON EXACTLINE SEARCH (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba del gradiente conjugado tipo fletcher-reeves con exactline search\n \n";
    filelog.open((prefix_name+std::string("conj_grad_fr_exactline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::ConjugateGradient(func, xinit, exacline, criteria2, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //GRADIENTE CONJUGADO CON FLETCHER REEVES CON SOFTLINE SEARCH (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba del gradiente conjugado tipo fletcher-reeves con softline search (numerico)\n \n";
    filelog.open((prefix_name+std::string("conj_grad_fr_softline_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::ConjugateGradient(func, xinit, softline, criteria2, filelog, true, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);
    
    //METODO DE NEWTON

    func->resetCalls();
    std::cout << "Prueba del metodo Newton \n \n";
    filelog.open((prefix_name+std::string("newton_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::NewtonMethod(func, xinit, criteria1, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DE NEWTON (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba del metodo Newton (numerico) \n \n";
    filelog.open((prefix_name+std::string("newton_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::NewtonMethod(func, xinit, criteria1, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO NEWTON AMORTIGUADO (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba del metodo Newton Amortiguado (Levenberg-Marquad) \n \n";
    filelog.open((prefix_name+std::string("damped_newt_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DampedNewton(func, xinit, criteria2, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO NEWTON AMORTIGUADO (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba del metodo Newton Amortiguado (Levenberg-Marquad) (numerico) \n \n";
    filelog.open((prefix_name+std::string("damped_newt_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DampedNewton(func, xinit, criteria2, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO QUASI NEWTON DFP SOFTLINE

    func->resetCalls();
    std::cout << "Prueba del metodo Quasi Newton version DFP con softline search \n \n";
    filelog.open((prefix_name+std::string("quasinewton_dfp_softline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::QuasiNewton(func, xinit, softline, criteria2, filelog, false);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO QUASI NEWTON DFP EXACTLINE

    func->resetCalls();
    std::cout << "Prueba del metodo Quasi Newton version DFP con exactline search \n \n";
    filelog.open((prefix_name+std::string("quasinewton_dfp_exactline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::QuasiNewton(func, xinit, exacline, criteria2, filelog, false);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO QUASI NEWTON BFGS SOFTLINE

    func->resetCalls();
    std::cout << "Prueba del metodo Quasi Newton version BFGS con softline search \n \n";
    filelog.open((prefix_name+std::string("quasinewton_bfgs_softline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::QuasiNewton(func, xinit, softline, criteria2, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO QUASI NEWTON BFGS EXACTLINE

    func->resetCalls();
    std::cout << "Prueba del metodo Quasi Newton version BFGS con exactline search \n \n";
    filelog.open((prefix_name+std::string("quasinewton_bfgs_exactline_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::QuasiNewton(func, xinit, exacline, criteria2, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO QUASI NEWTON DFP SOFTLINE (GRADIENTE NUMERICO)

    func->resetCalls();
    std::cout << "Prueba del metodo Quasi Newton version DFP con softline search (numerico)\n \n";
    filelog.open((prefix_name+std::string("quasinewton_dfp_softline_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::QuasiNewton(func, xinit, softline, criteria2, filelog, false, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO QUASI NEWTON BFGS SOFTLINE (GRADIENTE NUMERICO)

    func->resetCalls();
    std::cout << "Prueba del metodo Quasi Newton version BFGS con softline search (numerico) \n \n";
    filelog.open((prefix_name+std::string("quasinewton_bfgs_softline_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::QuasiNewton(func, xinit, softline, criteria2, filelog, true, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    

    print_alg_brief(extime, fcalls, itermem);

    delete criteria1;
    delete criteria2;
    delete softline;
    delete noline;
    delete exacline;
    delete interline;
    delete btline;
}

void test_all2(VectorialFunction* func, double* xinit, std::string prefix_name, bool quiet){
    std::vector<int> itermem;
    std::vector<double> extime;
    std::vector<int> fcalls;
    
    Optim::quiet = quiet;

    std::ofstream filelog;

    timespec inicio, fin;
    
    //OPTIMIZACION CON LEVENBER-MARQUAD (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba de Levenberg-Marquad (Analitico)\n \n";
    filelog.open((prefix_name+std::string("levmarq_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::LevenbergMarquad(func, xinit, NULL, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DE LEVENBERG-MARQUAD (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba de Levenberg-Marquad (Numerico)\n \n";
    filelog.open((prefix_name+std::string("levmarq_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::LevenbergMarquad(func, xinit, NULL, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DE LEVENBERG-MARQUAD SECANTE

    func->resetCalls();
    std::cout << "Prueba de Levenberg-Marquad Secante\n \n";
    filelog.open((prefix_name+std::string("levmarq_secant.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::LevenbergMarquadSecant(func, xinit, NULL, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);


    //METODO DOG LEG CON FACTORIZACION QR (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba de DogLeg con factorizacion QR (Analitco)\n \n";
    filelog.open((prefix_name+std::string("dogleg_qr_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DogLegQR(func, xinit, NULL, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DOG LEG CON SVD (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba de DogLeg con SVD (Analitco)\n \n";
    filelog.open((prefix_name+std::string("dogleg_svd_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DogLegSVD(func, xinit, NULL, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DOG LEG CON FACTORIZACION LU (ANALITICO)

    func->resetCalls();
    std::cout << "Prueba de DogLeg con factorizacion LU (Analitco)\n \n";
    filelog.open((prefix_name+std::string("dogleg_lu_analit.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DogLegLU(func, xinit, NULL, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DOG LEG CON FACTORIZACION QR (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba de DogLeg con factorizacion QR (Numerico)\n \n";
    filelog.open((prefix_name+std::string("dogleg_qr_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DogLegQR(func, xinit, NULL, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DOG LEG CON SVD (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba de DogLeg con SVD (Numerico)\n \n";
    filelog.open((prefix_name+std::string("dogleg_svd_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DogLegSVD(func, xinit, NULL, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    //METODO DOG LEG CON FACTORIZACION LU (NUMERICO)

    func->resetCalls();
    std::cout << "Prueba de DogLeg con factorizacion LU (Numerico)\n \n";
    filelog.open((prefix_name+std::string("dogleg_lu_num.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DogLegLU(func, xinit, NULL, filelog, true);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);
    
    //METODO DOG LEG SECANTE

    func->resetCalls();
    std::cout << "Prueba de DogLeg Secante\n \n";
    filelog.open((prefix_name+std::string("dogleg_secant.dat")).c_str());
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &inicio);
    Optim::DogLegSecant(func, xinit, NULL, filelog);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin);
    filelog.close();
    print_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem);
    save_alg_data(cputime2ms(inicio, fin), func->getCalls(), Optim::iter_mem, extime, fcalls, itermem);

    print_alg_brief(extime, fcalls, itermem);

}

double cputime2ms(timespec& begin, timespec& end){
    return (double)(end.tv_sec - begin.tv_sec) * 1.0e3	 // segundos a milisegundos
							+ (double)(end.tv_nsec - begin.tv_nsec)/1.0e6;		// nanosegundos a milisegundos
}

void print_alg_data(double t, int calls, int iters){
    std::cout << "Tiempo empleado: " << t << " ms" << std::endl;
    std::cout << "Llamadas a función: " << calls << std::endl;
    std::cout << "Iteaciones del algoritmo: " << iters << std::endl << std::endl;
}

void save_alg_data(double ftime, int calls, int iters, std::vector<double>& extime, std::vector<int>& fcalls, std::vector<int>& siters){
    extime.push_back(ftime);
    fcalls.push_back(calls);
    siters.push_back(iters);
}

void print_alg_brief(std::vector<double>& extime, std::vector<int>& fcalls, std::vector<int>& siters){
    
    std::cout << "EXECUTION TIME VECTOR" << std::endl;
    for(unsigned int i = 0; i < extime.size(); i++){
        std::cout << extime[i] << std::endl;
    }

    std::cout << "FCALLS VECTOR" << std::endl;
    for(unsigned int i = 0; i < fcalls.size(); i++){
        std::cout << fcalls[i] << std::endl;
    }

    std::cout << "ITERATIONS VECTOR" << std::endl;
    for(unsigned int i = 0; i < siters.size(); i++){
        std::cout << siters[i] << std::endl;
    }
}

void test_function(FunctionND* func){
    double* x = func->reserveIn();
    double* g = func->reserveIn();
    double* H = func->reserveH();
    double F;

    std::cout << "VERIQUE CALCULOS ANALITICOS Y NUMERICOS DE LA FUNCIÓN \n \n";

    std::cout << "x = ( ";
    for(int i = 0; i < func->dimIn(); i++){
        x[i] = 1.0/(func->dimIn());
        std::cout << std::scientific << x[i] << " ";
    }
    std::cout << ") \n \n";

    func->eval(x, &F);
    std::cout << "Evaluación: " << F << std::endl << std::endl;

    func->jaco(x, g);
    std::cout << "Gradiente analitico: ( ";
    for(int i = 0; i < func->dimIn(); i++){
        std::cout << std::scientific <<  g[i] << " ";
    }
    std::cout << ") \n \n";

    func->hess(x, H);
    std::cout << "Hessiano analitico: ( ";
    for(int i = 0; i < func->dimIn()*func->dimIn(); i++){
        std::cout << std::scientific << H[i] << " ";
    }
    std::cout << ") \n \n";

    //func->eval(x, &F);

    func->jaco(x, g, true);
    std::cout << "Gradiente numerico: ( ";
    for(int i = 0; i < func->dimIn(); i++){
        std::cout << std::scientific << g[i] << " ";
    }
    std::cout << ") \n \n";

    func->hess(x, H, true);
    std::cout << "Hessiano numerico: ( ";
    for(int i = 0; i < func->dimIn()*func->dimIn(); i++){
        std::cout << std::scientific << H[i] << " ";
    }
    std::cout << ") \n \n";

    delete x;
    delete g;
    delete H;
}