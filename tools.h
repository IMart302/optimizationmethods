//****************************************************************************
//
//  Function definition of memory management for vectors and matrixs, save graphics files
//  and others
//
// Author       : Ricardo Legarda Saenz
// Language     : C++
// Compiler     : Borland C++ v5.0
// Environment  : Win32
// Revisions
//   Initial    : 14.08.2000
//   Last       : 25.04.2001
//
//****************************************************************************


#ifndef _MEMORIA_HPP
#define _MEMORIA_HPP     // include just once

// declaration of function used here
#include <iostream>



// funtion declaration for data type
void** matrix( unsigned long, unsigned long, unsigned long );
void free_matrix( void** );
void* vectorr( unsigned long, unsigned long );
void free_vectorr( void* );


// declaration for BLAS/LAPACK functions, double precision                  
struct _dcomplex { double re, im; };
typedef struct _dcomplex dcomplex;
extern "C"
{
  // BLAS 1, real
  double dnrm2_(const int&,const double*,const int&);
  double ddot_(const int&,const double*,const int&,const double*,const int&);
  void scopy_(const int&,const float*,const int&,const float*,const int&);
  void dcopy_(const int&,const double*,const int&,const double*,const int&);
  void dscal_(const int&,const double&,const double*,const int&);
  void daxpy_(const int&,const double&,const double*,const int&,const double*,const int&);
  void dgesv_(const int&, const int&, double*, const int&, const int*, const double*, const int&, const int&);
  int idamax_(const int&,const double*,const int&);

  // BLAS 2
  void dgemv_(const char&,const int&,const int&,const double&,const double*,const int&,const double*,const int&,const double&,const double*,const int&);
  void dgemm_(const char& TRANSA, const char& TRANSB, const int& M, const int& N, const int& K, const double& alpha, const double* A, const int& LDA, const double* B, const int& LDB, const double& beta, const double* C, const int& LDC);
  void dger_(const int& M, const int& N, const double& alpha, const double* x, const int& INCX, const double* Y, const int& INCY, const double* A, const int& LDA);
  void dsyrk_(const char&, const char&, const int&, const int&, const double&, const double*, const int&, const double&, const double*, const int&);
  void dposv_(const char&, const int&, const int&, const double*, const int&, const double*, const int&, const int&);
  void dgels_(const char&, const int&, const int&, const int&, const double*, const int&, const double*, const int&, const double*, const int&, const int&);
  void dgelss_(const int& M, const int& N, const int& NRHS, const double* A, const int& LDA, const double* B, const int& LDB, const double* S, const double& rcond, const int& rank, const double* work, const int& lwork, const int& info); 
  void dsymv_(const char*, const int&, const double&, const double*, const int&, const double*, const int&, const double&, const double*, const int&);
  void dsyr_(const char*, const int&, const double&, const double*, const int&, const double*, const int&);
  void dsyr2_(const char* UPLO, const int& N, const double& ALPHA, const double* X, const int& INCX, const double* Y, const int& INCY, const double* A, const int& LDA);
  void dgesvd_(const char& JOBU, const char& JOBVT, const int& M, const int& N, const double* A, const int& LDA, const double* S, const double* U, const int& LDU, const double* VT, const int& LDVT, const double* WORK, const int& LWORK, const int& info);

  void dpotrf_(const char&, const int&, const double*, const int&, const int&);
  void dpotrs_(const char&, const int&, const int&, const double*, const int&, const double*, const int&, const int&);
}


#endif
