#ifndef FUNCTION_H
#define FUNCTION_H

#include <cfloat>
#include <cmath>
#include "tools.h"

#define PI 3.14159265

//Funcion vectorial generalizada, solo funciona como clase abstracta o base
//para ser sobrecargada
class VectorialFunction{
protected:

    int dimin;
    int dimout;
    int calls;
    double* lasteval;
    double* lastjaco;
    double* stepsize;
    double* fneigbor;

public:

    VectorialFunction()
    {
        this->dimin = 1;
        this->dimout = 1;
        this->calls = 0;
        lasteval = new double[this->dimout];
        lastjaco = new double[this->dimin*this->dimout];
        stepsize = new double[this->dimin];
        fneigbor = new double[this->dimin];
    }

    VectorialFunction(int _dimin, int _dimout)
    {
        this->dimin = _dimin;
        this->dimout = _dimout;
        this->calls = 0;
        lasteval = new double[this->dimout];
        lastjaco = new double[this->dimin];
        stepsize = new double[this->dimin];
        fneigbor = new double[this->dimin];
    }

    virtual ~VectorialFunction(){
        delete[] lasteval;
        delete[] lastjaco;
        delete[] stepsize;
        delete[] fneigbor;
    }

    //Evaluacion virtual de la funcion, debe ser sobrecargada
    virtual void eval(double* x, double* f){
        this->calls++;
        for(int k = 0; k < this->dimout; k++){
            this->lasteval[k] = f[k];
        }
    }

    //Calcula el jacobiano de la funcion, debe ser sobrecargada
    virtual void jaco(double* x, double* j, bool numeric = false){
        //jacobiano numerico
        double eps = sqrt(DBL_EPSILON);
        double tmp, h;
        
        int N = this->dimin;
        int M = this->dimout;
        double* F = new double[M];
        dcopy_(M, this->lasteval, 1, F, 1);
        for(int c=0; c < N; c++){
            tmp = x[c];

            h = eps*(1.0 + fabs(tmp));
            x[c] = tmp + h;
            h = x[c] - tmp;

            //Func(param, x, &J[c*M]);
            this->eval(x, &j[c*M]);
            daxpy_(M, -1.0, F, 1, &j[c*M], 1);
            for(int r = 0; r < M; r++){
                j[r + c*M] /= h;
            }

            x[c] = tmp;
        }
        delete[] F;  //ERAS TU?
    }

    //Copia la ultima evaluacion de la funcion
    void lastEval(double* f){
        for(int n = 0; n < this->dimout; n++){
            f[n] = lasteval[n];
        }
    }

    int dimIn(){
        return this->dimin;
    }

    int dimOut(){
        return this->dimout;
    }

    void resetCalls(){
        this->calls = 0;
    }

    int getCalls(){
        return this->calls;
    }

    //Reserva un puntero con el tamaño del vector de variables
    double* reserveIn(){
        return new double[this->dimin];
    }

    //Reserva un puntero con el tamaño del vector de salida
    double* reserveOut(){
        return new double[this->dimout];
    }
};


//Funcion con dimension n de entrada y 1 de salida
class FunctionND : public VectorialFunction{
public: 
    FunctionND(int dim) : 
        VectorialFunction(dim, 1)
    {

    }

    double* reserveH(){
        return new double[this->dimin*this->dimin];
    }

    //Evaluacion virtual de la funcion, debe ser sobrecargada
    virtual void eval(double* x, double* f){
        VectorialFunction::eval(x, f); // LLama a la función base
    }

    //Calcula el gradiente de la funcion, debe ser sobrecargada para el caculo analitico
    //por defecto cacula el gradiente numerico
    virtual void jaco(double* x, double* j, bool numeric = false){
        double F = *this->lasteval;
        double eps = pow(DBL_EPSILON, 1.0/3.0);
        double tmp;
        for(int r = 0; r < this->dimIn(); r++){
            tmp = x[r];
            stepsize[r] = eps*(1.0 + fabs(tmp));
            x[r] = tmp + stepsize[r];
            stepsize[r] = x[r] - tmp;
            this->eval(x, &fneigbor[r]);
            j[r] = (fneigbor[r] - F)/stepsize[r];
            x[r] = tmp;
        }
        *this->lasteval = F; //because it was modified
    }

    //Calucula el hessiano de la funcion, solo disponible en este tipo de funcion
    //debe ser sobrecargada para el calculo analitico, por defecto debe ser numerico
    virtual void hess(double* x, double* h, bool numeric = false){
        int N = this->dimIn();
        double F = *this->lasteval;
        for(int r = 0; r < N; r++){
            double tmpr = x[r];
            x[r] = tmpr+ 2.0*stepsize[r];
            double frc;
            this->eval(x, &frc);
            h[r+N*r] = (frc - fneigbor[r] - fneigbor[r] + F)/(stepsize[r]*stepsize[r]);
            x[r] = tmpr + stepsize[r];
            for(int c = r+1; c < N; c++){
                double tmpc = x[c];
                x[c] = tmpc + stepsize[c];
                this->eval(x, &frc);
                h[r+N*c] = (frc -fneigbor[r] - fneigbor[c] + F)/(stepsize[r]*stepsize[c]);
                h[c+N*r]=h[r+N*c];
                x[c] = tmpc;
            }
            x[r] = tmpr;
        }
        *this->lasteval = F;
    }
};

// ========================
// FUNCIONES NO ABSTRACTAS
// ========================

//Funcion Rastrigin en dimension variable
class RastriginFunction : public FunctionND{
public:
    RastriginFunction(int dim): 
        FunctionND(dim)
    {

    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        *f = (10.0)*((double)this->dimIn());
        for(int i = 0; i < this->dimIn(); i++){
            *f = *f + ((x[i]*x[i]) - 10*cos(2*PI*x[i]));
        }
        
        FunctionND::eval(x, f); //Llama a funcion base
    }

    //Calcula el gradiente de la funcion
    //por defecto cacula el analitico
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            FunctionND::jaco(x, j);
        }
        else{
            //gradiente analitico
            for(int i = 0; i < this->dimIn(); i++){
                j[i] = (2.0*x[i]) + 20*PI*sin(2*PI*x[i]);
            }
        }
    }

    //Calucula el hessiano de la funcion
    //por defecto debe usar el analitico
    virtual void hess(double* x, double* h, bool numeric = false){
        if(numeric){
            FunctionND::hess(x, h);
        }
        else{
            //hessiano analitico
            for(int i = 0; i < this->dimIn()*this->dimIn(); i++){
                h[i] = 0;
            }
            for(int i = 0; i < this->dimIn(); i++){
                h[i*this->dimIn() + i] = 2.0 + 40.0*PI*PI*cos(2*PI*x[i]);
            }
        }
    }
};

class QF2Function : public FunctionND{

public: 
    QF2Function(int dim) : 
        FunctionND(dim)
    {

    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        *f = 0;
        for(int i = 0; i < this->dimIn(); i++){
            *f = *f + (((double)(i+1))*((x[i]*x[i] - 1.0)*(x[i]*x[i] - 1.0)));
            //*f = *f + ((((double)(i+1)) + 1.0)*((x[i]*x[i] - 1.0)*(x[i]*x[i] - 1.0) - x[this->dimin-1]));
        }
        *f = *f/2.0 - x[this->dimin - 1];

        FunctionND::eval(x, f); //Siempre llama la funcion base despues de evaluar
    }

    //Calcula el gradiente de la funcion
    //por defecto cacula el analitico
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            FunctionND::jaco(x, j);
        }
        else{
            //gradiente analitico
            for(int i = 0; i < this->dimIn(); i++){
                //j[i] = 2.0*((double)(i+1))*x[i]*(x[i]*x[i] - 1.0);
                j[i] = 2.0*((double)(i+1))*x[i]*(x[i]*x[i] - 1.0);
            }
            //j[this->dimIn()-1] = j[this->dimIn()-1] - ((double)this->dimin)/2.0;
            j[this->dimIn()-1] = j[this->dimIn()-1] - 1.0; 
        }
    }

    //Calucula el hessiano de la funcion, solo disponible en este tipo de funcion
    //por defecto debe usa el analitico
    virtual void hess(double* x, double* h, bool numeric = false){
        if(numeric){
            FunctionND::hess(x, h);
        }
        else{
            //hessiano analitico
            for(int i = 0; i < this->dimIn()*this->dimIn(); i++){
                h[i] = 0;
            }
            for(int i = 0; i < this->dimIn(); i++){
                h[i*this->dimIn() + i] = 2.0*((double)(i+1))*(3.0*x[i]*x[i] - 1.0);
            }
        }
    }
};

class Rosenbrock : public FunctionND{
private: 
    double a;
    double b;
public: 
    Rosenbrock(): 
        FunctionND(2)
    {
        a = 1.0;
        b = 100.0;
    }

    Rosenbrock(double aa, double bb): 
        FunctionND(2)
    {
        a = aa;
        b = bb;
    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        *f = (a - x[0])*(a - x[0]) + b*(x[1]-x[0]*x[0])*(x[1]-x[0]*x[0]);
        //*this->lasteval = *f; //recuerda copiar el valor por si se requiere el calculo numerico
        FunctionND::eval(x, f); //Siempre llama la funcion base despues de evaluar
    }

    //Calcula el gradiente de la funcion
    //por defecto cacula el analitico
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            FunctionND::jaco(x, j);
        }
        else{
            //gradiente analitico
            j[0] = -2.0*(a - x[0]) - 4.0*x[0]*b*(x[1]-x[0]*x[0]);
            j[1] = 2.0*b*(x[1]-x[0]*x[0]);
        }
    }

    //Calucula el hessiano de la funcion, solo disponible en este tipo de funcion
    //por defecto debe usa el analitico
    virtual void hess(double* x, double* h, bool numeric = false){
        if(numeric){
            FunctionND::hess(x, h);
        }
        else{
            //hessiano analitico
            h[0] = 2.0 + 400.0*(x[0]*x[0] - x[1]) + 800.0*x[0]*x[0];
	        h[1] = -400.0*x[0];
	        h[2] = -400.0*x[0];
	        h[3] = 200.0;
        }
    }
};

class Diag1Function : public FunctionND{
public: 
    Diag1Function(int dim):
        FunctionND(dim)
    {

    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        *f = 0;
        for(int i = 0; i < this->dimIn(); i++){
            *f = *f + (exp(x[i]) - ((double)(i+1))*x[i]);
        }
        FunctionND::eval(x, f); //Llama a funcion base
    }

    //Calcula el gradiente de la funcion
    //por defecto cacula el analitico
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            FunctionND::jaco(x, j, numeric);
        }
        else{
            //gradiente analitico
            for(int i = 0; i < this->dimIn(); i++){
                j[i] = exp(x[i]) - ((double)(i+1));
            }
        }
    }

    //Calucula el hessiano de la funcion
    //por defecto debe usar el analitico
    virtual void hess(double* x, double* h, bool numeric = false){
        if(numeric){
            FunctionND::hess(x, h, numeric);
        }
        else{
            //hessiano analitico
            for(int i = 0; i < this->dimIn()*this->dimIn(); i++){
                h[i] = 0;
            }
            for(int i = 0; i < this->dimIn(); i++){
                h[i*this->dimIn() + i] = exp(x[i]);
            }
        }
    }
};

class Zakharov : public FunctionND{
private: 
    double a;
    double b;
public: 
    Zakharov(int n): FunctionND(n){}

   

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        //*f = (a - x[0])*(a - x[0]) + b*(x[1]-x[0]*x[0])*(x[1]-x[0]*x[0]);
        //*this->lasteval = *f; //recuerda copiar el valor por si se requiere el calculo numerico
        int n = this->dimIn();
        //double a, b;
        a = b = 0.0;
        for (int i = 0; i < n; i++)
        {
            a += x[i]*x[i];
            b += (i+1)*x[i];
        }
        *f = a + (0.25*b*b) + 0.0625*b*b*b*b;

        FunctionND::eval(x, f); //Siempre llama la funcion base despues de evaluar
    }

    //Calcula el gradiente de la funcion
    //por defecto cacula el analitico
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            FunctionND::jaco(x, j);
        }
        else{
            int n = this->dimIn();
            a = b = 0.0;
            for (int i = 0; i < n; i++)
            {
                a += x[i]*x[i];
                b += (i+1)*x[i];
            }
            for (int i = 0; i < n; i++)
            {
                j[i] = 2*x[i] + b*(i+1) + 2*(0.5*b*0.5*b*0.5*b)*(i+1);
            }
        }
    }

    //Calucula el hessiano de la funcion, solo disponible en este tipo de funcion
    //por defecto debe usa el analitico
    virtual void hess(double* x, double* h, bool numeric = false){
        if(numeric){
            FunctionND::hess(x, h);
        }
        else{
            int N = this->dimIn();
            a = b = 0.0;
            for (int i = 0; i < N; i++)
            {
                a += x[i]*x[i];
                b += (i+1)*x[i];
            }
            for (int i = 0; i < N; i++)
            {
                if (((i*N+ i)%N) == 0)
                {
                    h[i*N+i] = 2 + 0.5*(i+1)*(i+1)*b + (6*(i+1)*(0.5*b*0.5*b));
                }
                else
                {
                    h[i*N+i] = 0.0;
                }
            }
        }
    }
};

//////FUNCIONES VECTORIALES PARA MINIMOS CUADRADOS NO LINEALES///////////

class VecExample32 : public VectorialFunction{

public:  
    VecExample32():
        VectorialFunction(2, 2)
    {

    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        
        f[0] = x[0];
        f[1] = 2.0*x[1]*x[1] + 10.0*x[0] / (x[0] + 0.1);

        VectorialFunction::eval(x, f);
    }

    //Jacobiano
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            VectorialFunction::jaco(x,j);
        }
        else{
            j[0] = 1.0;
            j[1] = 1.0 / ((x[0] + 0.1)*(x[0] + 0.1));
            j[2] = 0.0;
            j[3] = 4.0*x[1];
        }
    }
};

class RosenbrockVec : public VectorialFunction{
public:
    RosenbrockVec():
        VectorialFunction(2, 2)
    {

    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        
        f[0] = 10.0*(x[1] - x[0]*x[0]);
        f[1] = 1.0 - x[0];

        VectorialFunction::eval(x, f);
    }

    //Jacobiano
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            VectorialFunction::jaco(x,j);
        }
        else{
            j[0] = -20.0*x[0];
            j[1] = -1.0;
            j[2] = 10.0;
            j[3] = 0.0;
        }
    }
};

class ExponentialFit : public VectorialFunction{
private: 
    double* yitab;
public:

    virtual ~ExponentialFit(){
        delete yitab;
    }

    ExponentialFit():
        VectorialFunction(4, 45)
    {
        yitab = new double[this->dimout];
        
        yitab[0]= 0.090542;  yitab[15]=0.288903;  yitab[30]=0.150874;
        yitab[1]= 0.124569;  yitab[16]=0.300820;  yitab[31]=0.126220;
        yitab[2]= 0.179367;  yitab[17]=0.303974;  yitab[32]=0.126266;
        yitab[3]= 0.195654;  yitab[18]=0.283987;  yitab[33]=0.106384;
        yitab[4]= 0.269707;  yitab[19]=0.262078;  yitab[34]=0.118923;
        yitab[5]= 0.286027;  yitab[20]=0.281593;  yitab[35]=0.091868;
        yitab[6]= 0.289892;  yitab[21]=0.267531;  yitab[36]=0.128926;
        yitab[7]= 0.317475;  yitab[22]=0.218926;  yitab[37]=0.119273;
        yitab[8]= 0.308191;  yitab[23]=0.225572;  yitab[38]=0.115997;
        yitab[9]= 0.336995;  yitab[24]=0.200594;  yitab[39]=0.105831;
        yitab[10]= 0.348371; yitab[25]=0.197375;  yitab[40]=0.075261;
        yitab[11]= 0.321337; yitab[26]=0.182440;  yitab[41]=0.068387;
        yitab[12]= 0.299423; yitab[27]=0.183892;  yitab[42]=0.090823;
        yitab[13]= 0.338972; yitab[28]=0.152285;  yitab[43]=0.085205;
        yitab[14]= 0.304763; yitab[29]=0.174028;  yitab[44]=0.067203;

    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        double t;
        for(int i = 0; i < this->dimout; i++){
            t = (double)(i+1);
            f[i] = yitab[i] - (x[2]*exp(x[0]*0.02*t) + x[3]*exp(x[1]*0.02*t));
        }

        VectorialFunction::eval(x, f);
    }

    //Jacobiano
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            VectorialFunction::jaco(x,j);
        }
        else{
            double i;
            for(int r = 0; r < this->dimout; r++){
                i = (double)(r+1);
                j[r] = -x[2]*exp(x[0]*0.02*i)*(0.02*i);
                j[r+this->dimout] = -x[3]*exp(x[1]*0.02*i)*(0.02*i);
                j[r+2*this->dimout] = -exp(x[0]*0.02*i);
                j[r+3*this->dimout] = -exp(x[1]*0.02*i);
            }
        }
    }
};

class VecExample31 : public VectorialFunction {
public: 
    VecExample31():
        VectorialFunction(2,3)
    {

    }

    //Evaluacion de la funcion
    virtual void eval(double* x, double* f){
        f[0] = 10.0*(x[1] - x[0]*x[0]);
        f[1] = 1.0 - x[0];
        f[2] = 1;

        VectorialFunction::eval(x, f);
    }

    //Jacobiano
    virtual void jaco(double* x, double* j, bool numeric = false){
        if(numeric){
            VectorialFunction::jaco(x,j);
        }
        else{
            j[0] = -20.0*x[0];
            j[1] = -1.0;
            j[2] = 0;
            j[3] = 10.0;
            j[4] = 0;
            j[5] = 0;
        }
    }   

};



#endif