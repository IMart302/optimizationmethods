#http://www.cplusplus.com/forum/unices/12499/#msg59885
CC           = g++ -O2
CFLAGS   = -Wno-deprecated -Wall -ansi 
LDFLAGS = -lm -lblas -llapack -lrt


all: test clean

test: main.o tools.o
	$(CC) -o $@ $^ $(LDFLAGS)

main.o: maintest.cpp
	$(CC) -o $@ -c $(CFLAGS) $<

tools.o: tools.cpp tools.h
	$(CC) -c $(CFLAGS) $<

.PHONY: clean cleanest

clean:
	rm *.o

cleanest: clean
	rm test